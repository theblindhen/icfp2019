open Core

type point = int * int

type map = point list

type boosterCode = B | F | L | X | R | C

type boosterLocation = boosterCode * point

type obstacles = map list

type boosters = boosterLocation list

type task = map * point * obstacles * boosters * (boosterCode list)

module Private = struct
  let (@) str i =
    if i < String.length str
    then str.[i]
    else Char.of_int_exn 0

  let read_int str i =
    let rec from ~acc i =
      let c = str @ i in
      if c >= '0' && c <= '9'
      then from ~acc:(10*acc + (Char.to_int c - Char.to_int '0')) (i+1)
      else (acc, i)
    in
    from ~acc:0 i

  let read_point str i =
    match str @ i with
    | '(' ->
        let (x, i) = read_int str (i+1) in
        assert (str.[i] = ',');
        let (y, i) = read_int str (i+1) in
        assert (str.[i] = ')');
        Some ((x, y), i+1)
    | _ -> None

  let rec read_point_list str i =
    match read_point str i with
    | Some (point, i) ->
        begin match str @ i with
        | ',' ->
          let (tail, i) = read_point_list str (i+1) in
          (point :: tail, i)
        | _ -> ([point], i)
        end
    | None -> ([], i)

  let read_booster_code' = function
    | 'B' -> Some B
    | 'F' -> Some F
    | 'L' -> Some L
    | 'X' -> Some X
    | 'R' -> Some R
    | 'C' -> Some C
    | _ -> None

  let read_booster_code str i =
    match read_booster_code' (str @ i) with
    | Some c -> Some (c, i+1)
    | None -> None

  let rec read_obstacles str i =
    match read_point_list str i with
    | ([], i) -> ([], i)
    | (obstacle, i) ->
        begin match str @ i with
        | ';' ->
          let (tail, i) = read_obstacles str (i+1) in
          (obstacle :: tail, i)
        | _ -> ([obstacle], i)
        end

  let read_booster_location str i : (boosterLocation * int) option =
    match read_booster_code str i with
    | None -> None
    | Some (code, i) ->
        match read_point str i with
        | None -> assert false
        | Some (point, i) ->
            Some ((code, point), i)

  let rec read_boosters str i =
    match read_booster_location str i with
    | Some (boosterLocation, i) ->
        begin match str @ i with
        | ';' ->
          let (tail, i) = read_boosters str (i+1) in
          (boosterLocation :: tail, i)
        | _ -> ([boosterLocation], i)
        end
    | None -> ([], i)
end

open Private

let read_task str =
  let i = 0 in
  let (map, i) = read_point_list str i in
  assert (str @ i = '#');
  let (startPos, i) = read_point str (i+1) |> Option.value_exn in
  assert (str @ i = '#');
  let (obstacles, i) = read_obstacles str (i+1) in
  assert (str @ i = '#');
  let (boosters, i) = read_boosters str (i+1) in
  ignore i;
  (map, startPos, obstacles, boosters)

let read_buyfile str =
  String.to_list str
  |> List.filter ~f:((<>) '\n')
  |> List.map ~f:(fun ch ->
         match read_booster_code' ch with
         | None -> failwith "Illegal buy code"
         | Some a -> a)

let loadFile filename = In_channel.read_all filename

let loadProblem filename buyfile_opt =
  let (map, startPos, obstacles, boosters) = read_task (loadFile filename) in
  let buys =
    match buyfile_opt with
    | None -> []
    | Some buyfile -> read_buyfile (loadFile buyfile) in
  (map, startPos, obstacles, boosters, buys)
            

let%test "read_int" =
  read_int "a28b" 1 = (28, 3)

let%test "read_point" =
  read_point " (2,3)" 1 = Some ((2,3), 6)

let%test "read_point_list" =
  read_point_list " (2,3),(1,0)" 1 = ([(2,3);(1,0)], 12)

let%test "read_obstacles empty" =
  read_obstacles "##" 1 = ([], 1)

let%test "read_obstacles" =
  read_obstacles " (1,2);(3,4),(5,6)" 1 =
    ([ [(1,2)]; [(3,4);(5,6)] ], 18)

let%test "read_booster_location empty" =
  read_booster_location "##" 1 = None

let%test "read_booster_location" =
  read_booster_location "#B(9,19)" 1 = Some ((B, (9,19)), 8)

let%test "read_boosters" =
  read_boosters "#B(9,19);X(1,2)" 1 = ([(B, (9,19)); (X, (1,2))], 15)

let%test "read_task trivial" =
  read_task "#(1,0)##" = ([], (1,0), [], [])

let%test "read_task" =
  read_task "(20,22),(30,32)#(1,0)#(1,2);(3,4),(5,6)#B(9,19);X(1,2)" =
    (
      [20,22; 30,32],
      (1,0),
      [ [(1,2)]; [(3,4);(5,6)] ],
      [(B, (9,19)); (X, (1,2))]
    )
