open Core
open SolutionFile
open Model
open SolverUtil

(* Returns a _reverse_ list of actions and a destination coordinate. *)
let find_nearest_unwrapped map start_coord =
  let visited = Bitv.create (model_size map * model_size map) false in
  (* Invariant: `next` contains only unblocked coordinates *)
  let next = ref [] in
  Bitv.set visited (coord_to_idx map start_coord) true;
  let rec search current =
    let results =
      List.filter current ~f:(fun (actions_rev, coord) ->
        match is_wrapped map coord with
        | false -> true (* found a nearest unwrapped *)
        | true ->
            (* Add every new neighbor to `visited` and `queue` *)
            neighbors_with_action coord
            |> List.filter ~f:(fun (_, coord) ->
                is_unblocked map coord &&
                not (Bitv.get visited (coord_to_idx map coord))
              )
            |> List.iter ~f:(fun (action, coord) ->
                (* It's impotant to add it to `visited` already here, even
                 * though we haven't strictly "visited" it. Otherwise we risk
                 * adding it to the queue multiple times. *)
                Bitv.set visited (coord_to_idx map coord) true;
                next := ((action :: actions_rev), coord) :: !next
              );
            false
      )
    in
    match results with
    | _::_ -> results
    | [] ->
        begin match !next with
        | [] -> [] (* the whole map is wrapped: we're done *)
        | current ->
            next := [];
            search current
        end
  in
  search [([], start_coord)]

let list_max_elements ~compare =
  let rec keep_better_than ~first_best ~other_best ~tail =
    match tail with
    | [] -> first_best :: other_best
    | h::t ->
        let compared = compare first_best h in
        if compared = 0 (* equivalent *)
        then keep_better_than ~first_best ~other_best:(h :: other_best) ~tail:t
        else
          if compared > 0 (* first_best > h *)
          then keep_better_than ~first_best ~other_best ~tail:t
          else (* first_best < h *)
            keep_better_than ~first_best:h ~other_best:[] ~tail:t
  in
  function
  | [] -> []
  | x :: xs -> keep_better_than ~first_best:x ~other_best:[] ~tail:xs

let where_to_go state bot =
  let map = state.map in (* TODO *)
  (* Rate how attractive it is to wrap this coordinate. *)
  let rate_coord map ~bot_coord coord =
    match is_blocked map coord || not (Sim.is_reachable state bot_coord coord) with
    | true -> -1
    | false ->
        match is_wrapped map coord with
        | true -> -1
        | false ->
            List.sum (module Int) (neighbors coord) ~f:(fun coord ->
              match is_blocked map coord with
              | true -> 2
              | false ->
                  match is_wrapped map coord with
                  | true -> 3
                  | false -> 0
            )
  in
  (* Rate how attractive it is to put the bot on this coordinate. *)
  let rate_bot_placement map coord =
    List.sum (module Int) bot.arms ~f:(fun arm ->
      rate_coord map ~bot_coord:coord (move_coord coord arm)
    )
  in
  neighbors_with_action bot.coord
  |> List.filter ~f:(fun (_, coord) -> is_unblocked map coord)
  |> List.map ~f:(fun (action, coord) ->
      (action, coord, rate_bot_placement map coord)
    )
  |> List.filter ~f:(fun (_, _, rating) -> rating >= 0)
  |> list_max_elements ~compare:(fun (_, _, a) (_, _, b) -> Int.compare a b)
  |> List.map ~f:(fun (action, coord, _) -> ([action], coord))


let try_rotate (direction: [`e|`q]) map arms coord =
  let rotation =
    match direction with
    | `e -> rotate_dpos_cw
    | `q -> rotate_dpos_ccw
  in
  let arms = List.map arms ~f:rotation in
  let new_wrapped_count =
    List.count arms ~f:(fun arm ->
      let coord = Model.move_coord coord arm in
      is_unblocked map coord &&
      not (is_wrapped map coord) (* TODO: line of sight *)
    )
  in
  (arms, new_wrapped_count)

let find_eager_rotation map arms coord =
  let arms_e, new_e = try_rotate `e map arms coord in
  let _arms_q, new_q = try_rotate `q map arms coord in
  if new_e > 0
  then
    if new_e > new_q
    then Some E
    else Some Q
  else
    (* We don't apply the wrapping effect of the previous rotation before
      * rotating again. That's okay because there wasn't any effect since
      * `new_e` was 0. *)
    let _arms_ee, new_ee = try_rotate `e map arms_e coord in
    (* If it's a good idea to turn E twice, we do it just once. We'll
      * discover in the next step that we should turn again. *)
    if new_ee > 0
    then Some E
    else None

(* Returns whether it's unblocked and not wrapped in the direction where
 * `direction` takes us. *)
let is_clear_in_front_of =
  let deltasD = [1,-1; 1,0; 1,1] in
  let deltasW = List.map ~f:rotate_dpos_ccw deltasD in
  let deltasA = List.map ~f:rotate_dpos_ccw deltasW in
  let deltasS = List.map ~f:rotate_dpos_ccw deltasA in
  fun map coord direction ->
    let deltas =
      match direction with
      | A -> deltasA
      | W -> deltasW
      | D -> deltasD
      | S -> deltasS
      | _ -> failwith "is_clear_in_front_of: not a move"
    in
    List.for_all deltas ~f:(fun delta ->
      let coord = move_coord coord delta in
      is_unblocked map coord &&
      not (is_wrapped map coord)
    )

let get_side_moves map coord direction =
  let deltas =
    match direction with
    | W | S -> [A, (-1,0) ; D, (1,0)]
    | A | D -> [S, (0,-1) ; W, (0,1)]
    | _ -> failwith "get_side_moves: not a direction"
  in
  deltas
  |> List.map ~f:(fun (action, delta) -> (action, move_coord coord delta))
  |> List.filter ~f:(fun (_, coord) -> is_unblocked map coord)

let bot_proximity state coord =
  (* Note: this sum includes the bot itself, which always contributes a term of
   * 1.0. It's more convenient to just have this term than to filter it out. *)
  Stack.sum (module Float) state.bots ~f:(fun bot ->
    let dist = manhattan_distance bot.coord coord in
    1. /. (1. +. Float.of_int dist)
  )

type rotation_policy = [`none | `forward | `eager]

let solve ?(rotation=(`none : rotation_policy)) state : solution =
  let open Sim.Monad in
  let plan_actions_rev actions_rev =
    all_unit (List.rev_map ~f:Sim.action actions_rev)
  in
  let rec decide () =
    Sim.get_bot >>= fun bot ->
    if state.powerups.manipulators_collected > 0 then
      Sim.action (bot_extend_broom state bot) >>= fun () ->
      decide ()
    else
      let policy =
        let size = model_size state.map in
        [ ([B;C], size/3) ; ([X], size/2) ] in
      match find_nearest_booster state bot policy with
      | Some (final_coord, actions_rev) ->
         plan_actions_rev actions_rev >>= fun () ->
         begin match get_booster state.map final_coord with
         | Some X when state.powerups.clones_collected >= 1 ->
             Sim.clone (decide ()) >>= fun () ->
             decide ()
         | _ -> decide ()
         end
      | None ->
          match
            if rotation = `eager
            then find_eager_rotation state.map bot.arms bot.coord
            else None
          with
          | Some action ->
              Sim.action action >>= fun () ->
              decide ()
          | None ->
          (* TODO: Don't go to the nearest field if we can go one distance
           * further and wrap everything in between thanks to our arms. *)
          (* TODO: As a tie breaker between equally good neighbors, continue in
           * the direction we're going? *)
          match
            let plan_a = where_to_go state bot in
            let plan =
              match plan_a with
              | [] -> find_nearest_unwrapped state.map bot.coord
              | _ -> plan_a
            in
            plan
            (* tie breaker: move away from other bots *)
            |> List.min_elt ~compare:(fun (_, coord1) (_, coord2) ->
                Float.compare
                  (bot_proximity state coord1)
                  (bot_proximity state coord2)
                )
          with
          | None -> return ()
          | Some (actions_rev, final_coord) ->
              let actions_rev =
                match rotation, actions_rev with
                | `forward, last_move::other_moves ->
                    let rotations_rev =
                      match bot.dir, last_move with
                      | Up, W | Right, D | Down, S | Left, A -> []
                      | Up, D | Right, S | Down, A | Left, W -> [E]
                      | Up, A | Right, W | Down, D | Left, S -> [Q]
                      | Up, S | Right, A | Down, W | Left, D -> [E; E]
                      | _, (Z | E | Q | B (_, _) | F | L | R | T _ | C) ->
                          failwith "Action wasn't a move"
                    in
                    if is_clear_in_front_of state.map final_coord last_move
                    then last_move :: rotations_rev @ other_moves
                    else
                      let side_moves =
                        get_side_moves state.map final_coord last_move
                      in
                      begin match
                        List.find side_moves ~f:(fun (_action, coord) ->
                          is_clear_in_front_of state.map coord last_move
                        )
                      with
                      | Some (action, _coord) ->
                          action :: last_move :: rotations_rev @ other_moves
                      | None -> actions_rev
                      end
                | _, _ -> actions_rev
              in
              plan_actions_rev actions_rev >>= fun () ->
              decide ()
  in 
  let () = Sim.run state (decide ()) in
  Sim.validate state

let%test "list_max_elements 1" =
  list_max_elements ~compare:Int.compare [ 1; 2; 3; 3; 2 ] = [ 3; 3 ]

let%test "list_max_elements 2" =
  list_max_elements ~compare:Int.compare [ ] = [ ]

let%test "list_max_elements 3" =
  list_max_elements ~compare:Int.compare [ 3; 1; 1 ] = [ 3 ]

let%test "list_max_elements 4" =
  list_max_elements ~compare:Int.compare [ 3; 1; 3 ] = [ 3; 3 ]

let%test "list_max_elements 5" =
  list_max_elements ~compare:Int.compare [ 2; 3; 1 ] = [ 3 ]
