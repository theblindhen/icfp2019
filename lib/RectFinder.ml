open Core
open Model

let find_largest_rect_from_coord (Coord (x,y) : coord) (Map(r,free_map,_,_) as map) =
  let check dx dy  = Bitv.get free_map (coord_to_idx map (Coord (x+dx,y+dy))) in
  
  (* find length of row *)
  let row_width dy max_width = 
    let rec row_iter dx max_width =
      if dx < max_width && check dx dy 
      then row_iter (dx+1) max_width
      else dx in
    row_iter 0 max_width in

  (* Iterate through the rows to find the largest rect *)
  let rec col_iter dy max_width max_height =
    if dy >= max_height 
    then (max_width,dy)
    else
      let dy_width = row_width dy max_width in
      if dy_width >= max_width
      then col_iter (dy+1) max_width max_height
      else 
        if dy_width * (dy+1) > dy * max_width
        then col_iter (dy+1) dy_width max_height
        else (max_width, dy) in
  let (width,height) = col_iter 1 (row_width 0 (r-x)) (r-y) in
  {llc= Coord (x,y); width= width; height= height}

let find_largest_unblocked_rect (Map(r,_,_,_) as map : map) : rectangle =
  let best = ref {llc = Coord (0,0); width = 0; height =  0} in
  for x = 0 to r-1 do
    for y = 0 to r-1 do
      let this = find_largest_rect_from_coord (Coord (x,y)) map in
      if this.width * this.height > !best.width * !best.height
      then best := this
    done
  done;
  !best

let rec find_largest_unblocked_rects (Map(r,free_map,i,b) as map : map) (min_size : int) =
  let rect = find_largest_unblocked_rect map in
  let Coord (x,y) = rect.llc in
  if rect.width * rect.height < min_size
  then []
  else 
    let new_free_map = Bitv.copy free_map in
    let new_map = Map(r,new_free_map,i,b) in
    for x_i =  x to x + rect.width - 1 do
      for y_i = y to y + rect.height - 1 do
        Bitv.set new_free_map (coord_to_idx map (Coord (x_i,y_i))) false
      done
    done;
    rect :: find_largest_unblocked_rects new_map min_size
