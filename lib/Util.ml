open Core
   
let print_it ?mes tosexp myval =
  match mes with
    | None -> Printf.printf "%s\n" (Sexp.to_string (tosexp myval));
    | Some m -> Printf.printf "%s%s\n" m (Sexp.to_string (tosexp myval));
  
