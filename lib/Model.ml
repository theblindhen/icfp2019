open Core
include SolutionFile
      
module IntPair = struct
  module T = struct
    type t = int * int [@@deriving compare, hash, sexp]
  end
  include T
  include Hashable.Make (T)
  include Comparable.Make (T)
end

type rectangle = { llc : coord;
                   width : int;
                   height : int } [@@deriving compare, hash, sexp]

type direction = Up | Down | Left | Right

type booster =  B | F | L | X | R | C
type boostertbl = booster Int.Table.t
type boostermap = BoosterMap of Bitv.t * boostertbl

type map = Map of int * Bitv.t * Bitv.t * boostermap (* size , map of is free space, map of is wrapped *)

type powerups = {
    mutable manipulators_collected: int;
    mutable fast_collected: int;
    mutable drill_collected: int;
    mutable teleport_sites: coord list;
    mutable teleports_collected: int;
    mutable clones_collected: int;
  }

type bot = {
  mutable coord: coord;
  mutable dir: direction;
  mutable arms: (int * int) list; (* includes bot's position *)
  actions: action Stack.t;
  mutable fast_active: int;
  mutable drill_active: int;
  id: int;
}

type state = {
  bots: bot Stack.t;
  map: map;
  powerups : powerups;
  mutable clock: int;
}

let new_map (r : int) =
  let boostermap = BoosterMap (Bitv.create (r * r) false, Int.Table.create ()) in
  Map (r, Bitv.create (r * r) false,  Bitv.create (r * r) false, boostermap)

let model_size (Map (r,_,_,_) : map) = r

let copy_map (Map (r, block, wrap, boost)) =
  (* TODO: Perhaps a make a copy function that only copies the wrapped space *)
  let boost' =
    let BoosterMap (bmap, btbl) = boost in
    BoosterMap (Bitv.copy bmap, Int.Table.copy btbl) in
  Map (r, Bitv.copy block, Bitv.copy wrap, boost')

let copy_state (state : state) =
  let { bots; map; powerups; clock=_ } = state in
  { state with
      map = copy_map map;
      bots = Stack.copy bots;
      powerups = {
        (* Poor man's record clone *)
        powerups with
            manipulators_collected = powerups.manipulators_collected
      };
  }

let string_of_coord (Coord (x,y)) = Printf.sprintf "(%d,%d)" x y

let coord_of_pair (x,y) = Coord (x,y)

let coord_to_idx (Map (r, _, _,_) : map) (Coord (x,y) : coord) =
  x * r + y

let is_within_bounding_box (m: map) (c: coord) =
  let r = model_size m in
  let Coord (x,y) = c in
  x >= 0 && y >= 0 && x < r && y < r

let neighbors (Coord (x,y) : coord) =
    [ Coord (x+1,y); Coord (x,y+1); Coord (x-1, y); Coord (x, y-1) ]

let neighbors_with_action (Coord (x,y) : coord) =
  [
    D, Coord (x+1,y);
    W, Coord (x,y+1);
    A, Coord (x-1, y);
    S, Coord (x, y-1)
  ]

let is_blocked (m : map) (c : coord) =
  let Map (_,block,_,_) = m in
  not (is_within_bounding_box (m: map) (c: coord)) ||
  not (Bitv.get block (coord_to_idx m c))

let is_unblocked (m : map) (c : coord) =
  not (is_blocked m c)

let is_wrapped (m : map) (c : coord) =
  let Map (_,_,wrap,_) = m in
  is_within_bounding_box m c &&
  Bitv.get wrap (coord_to_idx m c)

let should_be_wrapped (m : map) (c : coord) =
  is_unblocked m c && not (is_wrapped m c)

let set_blocked (m : map) (c : coord) =
  let Map (_,block,_,_) = m in
  if is_within_bounding_box m c then
    Bitv.set block (coord_to_idx m c) false

let set_unblocked (m : map) (c : coord) =
  let Map (_,block,_,_) = m in
  if is_within_bounding_box m c then
    Bitv.set block (coord_to_idx m c) true

let set_wrapped (m : map) (c : coord) =
  let Map (_,_,wrap,_) = m in
  if is_unblocked m c then
    Bitv.set wrap (coord_to_idx m c) true

(* True iff there's a booster on location *)
let has_booster (m : map) (location : coord) =
  let Map (_,_,_,BoosterMap (bmap, _)) = m in
  match is_within_bounding_box m location with
    | false -> false
    | _ ->
       let idx = coord_to_idx m location in
       Bitv.get bmap idx
  
(* Return a booster option for a booster on location *)
let get_booster (m : map) (location : coord) =
  let Map (_,_,_,BoosterMap (bmap, btbl)) = m in
  match is_within_bounding_box m location with
    | false -> None
    | _ ->
       let idx = coord_to_idx m location in
       match Bitv.get bmap idx with
       | true -> Hashtbl.find btbl idx
       | false -> None

let remove_booster (m : map) (location : coord) =
  let Map (_,_,_,BoosterMap (bmap, btbl)) = m in
  if is_within_bounding_box m location then begin
    let idx = coord_to_idx m location in
    Bitv.set bmap idx false;
    Hashtbl.remove btbl idx
    end

let move_coord (Coord (x,y) : coord) ((dx, dy) : (int * int)) =
  Coord (x+dx, y+dy)

let move_coord_in_dir (Coord (x,y) : coord) = function
    | Up -> Coord (x, y+1)
    | Right -> Coord (x+1, y)
    | Down -> Coord (x, y-1)
    | Left -> Coord (x-1, y)

let manhattan_distance (Coord (x1,y1) : coord) (Coord (x2,y2) : coord) =
  Int.abs(x1-x2) + Int.abs(y1-y2)

let reached_positions (bot : bot) =
  (* TODO: Line-of-sight *)
  List.map bot.arms ~f:(move_coord bot.coord)

let rotate_direction_cw = function
    Up -> Right | Right -> Down | Down -> Left | Left -> Up

let rotate_direction_ccw = function
    Up -> Left | Left -> Down | Down -> Right | Right -> Up

let rotate_dpos_cw (x,y) = (y, -x)
let rotate_dpos_ccw (x,y) = (-y, x)

let rotate_dpos_from_right_to_dir dpos target_dir =
  match target_dir with
    | Right -> dpos
    | Up -> rotate_dpos_ccw dpos
    | Down -> rotate_dpos_cw dpos
    | Left -> rotate_dpos_cw (rotate_dpos_cw dpos)

let find_random_coord (size : int) (needle : coord -> bool) =
  let rec find' () =
    let c = Coord (Random.int size , Random.int size) in
    if needle c then
      c
    else
      find' () in
  find' ()

(* Find something you need in the vicinity.
 * Returns (final coord, action list, dist) *)
let astar_to_something (start_coord : coord) (radius : int) (coord_passable : coord -> bool) (heuristic : (coord * action list * int) -> int) (needle : coord -> bool) =
  let visited = Coord.Hash_set.create () in
  let queue = Heap.create ~cmp:(fun a1 a2  -> Int.compare (heuristic a1) (heuristic a2)) ()  in
  (* `queue` contains only passable coordinates *)
  Hash_set.add visited start_coord;
  Heap.add queue (start_coord, [], 0);
  let rec search () =
    match Heap.pop queue with
    | None -> None (*  *)
    | Some (coord, actions_rev, d) ->
        match needle coord, d < radius with
        | true,_ ->
           Some (coord, List.rev actions_rev, d) (* found nearest needle *)
        | false, true ->
            (* Add every new neighbor to `visited` and `queue` *)
           neighbors_with_action coord
           |> List.filter ~f:(fun (_, coord) ->
                  coord_passable coord && not (Hash_set.mem visited coord))
           |> List.iter ~f:(fun (action, coord) ->
                  (* It's impotant to add it to `visited` already here, even
                   * though we haven't strictly "visited" it. Otherwise we risk
                   * adding it to the queue multiple times. *)
                  Hash_set.add visited coord;
                  Heap.add queue (coord, (action :: actions_rev), d+1)
                );
           search ()
        | _, false -> search () (* Out of range *)
  in
  search ()

(* Find something you need in the vicinity.
 * Returns (final coord, action list, dist) option *)
let find_near_manhattan (start_coord : coord) (radius : int) (coord_passable : coord -> bool) (needle : coord -> bool) =
  astar_to_something start_coord radius coord_passable (fun (_,_,d) -> d) needle

(* Find a shortest path through "passable" coords from a single start to a single target.
 * Returns (final coord, action list, dist) option *)
let path_to_target (start_coord : coord) (coord_passable : coord -> bool) (target_coord : coord) =
  astar_to_something start_coord Int.max_value coord_passable (fun (c,_,_) -> -manhattan_distance c target_coord) ((=) target_coord)


let unit_vector_of_movement (move : action) =
  match move with
  | W -> ( 0, 1) (* move up *)
  | S -> ( 0,-1) (* move down *)
  | A -> (-1, 0) (* move left *)
  | D -> ( 1, 0) (* move right *)
  | _ -> failwith "Not a movement action"

(* Return a path from applying a list of movement actions to a start coord.
 * Does not include the starting coord itself, but does include end coord *)
let coord_list_of_actions (start : coord) (actions: action list) =
  let _, coords_rev =
    actions |>
    List.fold ~init:(start, []) ~f:(fun (coord, acc) move ->
        let next = move_coord coord (unit_vector_of_movement move) in
        (next, next::acc))
  in
  List.rev (coords_rev)

(* Make a rectangle of coordinates such that it has 'width' rectangles in the
 * x-axis and 'height' rectangles in the y-axis, and 'llc' is the lower-left-hand
 * corner *)
let coords_of_rect (r : rectangle) : coord list =
  List.init r.width ~f:(fun dx ->
    List.init r.height ~f:(fun dy ->
        move_coord r.llc (dx, dy)))
  |> List.concat


module Private = struct

  let coord_to_idx' (Map (r, _, _,_) : map) ((x,y) : (int * int)) =
    x * r + y

  let idx_to_coord (Map (r, _, _,_) : map) (i : int) =
    let y = i % r in
    let x = (i - y) / r in
    Coord (x, y)

  let neighbors' (Map (r, _, _, _) : map) ((x,y) : int * int) =
    [ (x+1,y); (x,y+1); (x-1, y); (x, y-1) ]
    |> List.filter ~f:(fun (x,y) -> x >= 0 && y >= 0 && x < r && y < r)

  let is_blocked' (m : map) (xy : int * int) =
    let Map (_,block,_,_) = m in
    not (Bitv.get block (coord_to_idx' m xy))

  let is_unblocked' (m : map) (xy : int * int) =
    let Map (_,block,_,_) = m in
    Bitv.get block (coord_to_idx' m xy)

  let is_wrapped' (m : map) (xy : int * int) =
    let Map (_,_,wrap,_) = m in
    Bitv.get wrap (coord_to_idx' m xy)

  let should_be_wrapped' (m : map) (xy : int * int) =
    is_unblocked' m xy && not (is_wrapped' m xy)

  let set_blocked' (m : map) (xy : int * int) =
    let Map (_,block,_,_) = m in
    Bitv.set block (coord_to_idx' m xy) false

  let set_unblocked' (m : map) (xy : int * int) =
    let Map (_,block,_,_) = m in
    Bitv.set block (coord_to_idx' m xy) true

  let set_wrapped' (m : map) (xy : int * int) =
    (* NOTE: This does not check for blocked *)
    let Map (_,_,wrap,_) = m in
    Bitv.set wrap (coord_to_idx' m xy) true

  let set_booster (m : map) (booster : booster) (location : coord) =
    let Map (_,_,_,BoosterMap (bmap, btbl)) = m in
    let idx = coord_to_idx m location in
    Bitv.set bmap idx true;
    Hashtbl.set btbl ~key:idx ~data:booster
    
  let point_on_contour_segment (pstart : int * int) (pend : int * int) =
    let (sx, sy) = pstart in
    let (ex, ey) = pend in
    if sy = ey then
      (if sx < ex then
         (sx, sy)
       else
         (sx - 1, sy-1))
    else
      (if sy < ey then
         (sx-1, sy)
       else
         (sx, sy-1))

  let crosses_contour_segment (x1,y1) (x2,y2) ((sx1, sy1),(sx2,sy2)) =
    let ans = 
      let max a b = if a < b then b else a in
      let min a b = if a < b then a else b in
      if x1 = x2 && sy1 = sy2 then
        x1 >= min sx1 sx2 && x1 < max sx1 sx2 && sy1 = max y1 y2
      else if y1 = y2 && sx1 = sx2 then
        y1 >= min sy1 sy2 && y1 < max sy1 sy2 && sx1 = max x1 x2
      else
        false
    in
    ans

  let flood_blocking_in_contour (map : map) (contour : (int * int) list) (unblock : bool) =
    let starting_point, cont_segms =
      match contour with
      | p1::p2::cpoints -> 
         let starting_point = point_on_contour_segment p1 p2 in
         let csegms = List.zip_exn contour (p2::cpoints @ [p1]) in
         (starting_point, csegms)
      | _ -> failwith "Invalid contour with less than two points" in
    let is_active = if unblock then is_unblocked' else is_blocked' in
    let activate  = if unblock then set_unblocked' else set_blocked' in
    let crosses_contour p1 p2 =
      (*TODO: This is not asymptotically fast *)
      List.exists cont_segms ~f:(crosses_contour_segment p1 p2) in
    let rec flood_in_contour xy =
      if not (is_active map xy) then (
        activate map xy;
        List.iter (neighbors' map xy) ~f:(fun npos ->
            if not (crosses_contour xy npos) then
              flood_in_contour npos)
      ) in
    flood_in_contour starting_point

end
open Private (* Locally visible *)

let iter_over_unblocked (state : state) (f : coord -> unit) =
  let Map (_,freemap,_,_) = state.map in
  Bitv.iteri (fun idx unblocked ->
      if unblocked then f (idx_to_coord state.map idx)) freemap

(* Returns the max x and max y of the free squares *)
let model_true_size (Map (_,unblocks,_,_) as map : map) =
  unblocks |> Bitv.foldi_left (fun (maxx, maxy) idx is_unblocked ->
                 match is_unblocked with
                 | false -> (maxx, maxy)
                 | true ->
                    let Coord (x,y) = idx_to_coord map idx in
                    (Int.max maxx x, Int.max maxy y)) (0,0)

let boosters_left (m : map) =
  let Map (_,_,_,BoosterMap (_,btbl)) = m in
  Hashtbl.to_alist btbl
  |> List.map ~f:(fun (idx, code) -> (code, idx_to_coord m idx))

let any_satisfactory_boosters_left (boosters_left: (booster * coord) list) (needle : booster*coord -> bool) =
  List.exists boosters_left ~f:needle

let state_from_task (task : TaskFile.task) =
  let (contour, (startx, starty), obstacles, boosters, buys) = task in
  let coord = Coord (startx, starty) in
  let size = List.fold contour ~init:0 ~f:(fun size (x,y) ->
                 let xymax = if x > y then x else y in
                 if size > xymax then size else xymax) in
  let map = new_map size in
  flood_blocking_in_contour map contour true;
  List.iter obstacles ~f:(fun ocontour ->
      flood_blocking_in_contour map ocontour false
    );
  List.iter boosters ~f:(fun (bcode, (x,y)) ->
      let booster = match bcode with
        | TaskFile.B -> B | TaskFile.F -> F | TaskFile.L -> L | TaskFile.X -> X
        | TaskFile.R -> R | TaskFile.C -> C
      in
      set_booster map booster (Coord (x,y))
    );
  let arms = [ (0,0); (1,0); (1,1); (1,-1) ] in
  List.iter arms ~f:(fun (dx,dy) ->
      set_wrapped map (move_coord coord (dx,dy)));
  let pows = { manipulators_collected = 0;
               fast_collected = 0; drill_collected = 0;
               teleport_sites = []; teleports_collected = 0;
               clones_collected = 0;
             } in
  List.iter buys ~f:(fun c ->
      match c with
      | TaskFile.B -> pows.manipulators_collected <- pows.manipulators_collected + 1
      | TaskFile.F -> pows.fast_collected <- pows.fast_collected + 1
      | TaskFile.L -> pows.drill_collected <- pows.drill_collected + 1
      | TaskFile.R -> pows.teleports_collected <- pows.teleports_collected + 1
      | TaskFile.C -> pows.clones_collected <- pows.clones_collected + 1
      | TaskFile.X -> failwith "I can't carry that!");
  let bot =
    { coord; arms; dir = Right; actions = Stack.create ();
      fast_active = 0; drill_active = 0; id=0; }
  in
  { map= map; bots= Stack.singleton bot;
    powerups= pows; clock= 0 }

let string_of_booster = function
    | B -> "B" | F -> "F" | L -> "L" | X -> "X"
    | R -> "R" | C -> "C"
   
let string_of_state state = 
  let Map (r,_,_,_) = state.map in
  let endlines = "*"^ (String.init r ~f:(fun _ -> '-')) ^"*" in
  let body = 
    List.init r ~f:(fun negy ->
        let y = r - negy - 1 in
        let paint_pos =
          Stack.to_list state.bots
          |> List.concat_map ~f:reached_positions
        in
        let body = 
            List.init r ~f:(fun x ->
                let pos = Coord (x,y) in
                if is_blocked state.map pos then
                    "#"
                else if
                  Stack.exists state.bots ~f:(fun { coord; _ } -> coord = pos)
                then
                    "&"
                else if List.exists paint_pos ~f:(fun p -> p = pos) then
                    "@"
                    (* TODO: Draw the reach/dir of the robot *)
                else if is_blocked state.map pos then
                    "#"
                else if is_wrapped state.map pos then
                    "·"
                else
                    match get_booster state.map pos with
                    | None -> " "
                    | Some b -> string_of_booster b
                )
            |> String.concat in
        "|"^ body ^"|"
        )
    |> String.concat ~sep:"\n" in
  let powerups =
    let p = state.powerups in
    Printf.sprintf "Collected powerups:\tFast=%i\tDrill=%i\tTeleports=%i\n"
       p.fast_collected p.drill_collected p.teleports_collected
    ^(match p.teleport_sites with
      | [] -> ""
      | _ -> "TODO: Some teleport beacons dropped\n")
  in
  Printf.sprintf "Map: %d x %d. Clock: %d\n" (model_size state.map) (model_size state.map) state.clock
  ^endlines ^"\n"^ body ^"\n"^ endlines ^"\n"^ powerups


   


let%test "point_on_contour_segment_right" =
  point_on_contour_segment (0,0) (3,0) =  (0,0) 

let%test "point_on_contour_segment_left" =
  point_on_contour_segment (0,0) (-3,0) =  (-1,-1) 

let%test "point_on_contour_segment_up" =
  point_on_contour_segment (0,0) (0,3) =  (-1,0) 

let%test "point_on_contour_segment_down" =
  point_on_contour_segment (0,0) (0,-3) =  (0,-1) 

let%test "crosses_contour_segment0x" =
  crosses_contour_segment (0,0) (0,1) ((0,1),(10,1)) = true
let%test "crosses_contour_segment1x" =
  crosses_contour_segment (9,0) (9,1) ((0,1),(10,1)) = true
let%test "crosses_contour_segment2x" =
  crosses_contour_segment (10,0) (10,1) ((0,1),(10,1)) = false
let%test "crosses_contour_segment3x" =
  crosses_contour_segment (-1,0) (-1,1) ((0,1),(10,1)) = false
let%test "crosses_contour_segment4x" =
  crosses_contour_segment (0,1) (0,2) ((0,1),(10,1)) = false
let%test "crosses_contour_segment5x" =
  crosses_contour_segment (0,-1) (0,0) ((0,1),(10,1)) = false
let%test "crosses_contour_segment0y" =
  crosses_contour_segment (0,0) (1,0) ((1,0),(1,10)) = true
let%test "crosses_contour_segment1y" =
  crosses_contour_segment (0,9) (1,9) ((1,0),(1,10)) = true
let%test "crosses_contour_segment2y" =
  crosses_contour_segment (0,10) (1,10) ((1,0),(1,10)) = false
let%test "crosses_contour_segment3y" =
  crosses_contour_segment (0,-1) (1,-1) ((1,0),(1,10)) = false
let%test "crosses_contour_segment4y" =
  crosses_contour_segment (1,0) (2,0) ((1,0),(1,10)) = false
let%test "crosses_contour_segment5y" =
  crosses_contour_segment (-1,0) (0,0) ((1,0),(1,10)) = false

let%test "rotate_dpos_cw1" = rotate_dpos_cw (0,1) = (1,0)
let%test "rotate_dpos_cw2" = rotate_dpos_cw (1,2) = (2,-1)
let%test "rotate_dpos_ccw1" = rotate_dpos_ccw (1,0) = (0,1)
let%test "rotate_dpos_ccw2" = rotate_dpos_ccw (2,-1) = (1,2)

let%test "rotate_direction" =
  List.for_all [Up; Down; Left; Right] ~f:(fun d -> rotate_direction_ccw (rotate_direction_cw d) = d)
