open Core
open Model
open SolutionFile

let diff_coor (Coord (ax,ay)) (Coord (bx,by)) =
  (bx-ax, by-ay) 

let dneighbors (dx,dy) =
  [(dx-1,dy); (dx+1,dy); (dx,dy-1); (dx,dy+1)]

let rec gcd a b = 
  if b = 0 
  then a 
  else gcd b (a mod b)

let is_reachable_range (s : state) (a : coord) (ex,ey : int*int) (dist : int) =
  List.init (dist+1) ~f:(fun i -> move_coord a (ex*i, ey*i))
  |> List.for_all ~f:(is_unblocked s.map)

let is_reachable (s : state) (a : coord) (b : coord) =
  if a = b then true
  else
    let (dx,dy) = diff_coor a b in
    if not (Int.abs(dx) = 1 || Int.abs(dy) = 1) then
      failwith "Nasty broom"
    else
      let is_wide = Int.abs(dy) = 1 in
      let (ex, ey, dist) =
        if is_wide then
          ((if dx > 0 then 1 else -1), 0, Int.abs(dx)/2)
        else
          (0, (if dy > 0 then 1 else -1), Int.abs(dy)/2) in
      is_reachable_range s a (ex, ey) dist && is_reachable_range s b (-ex, -ey) dist

let wrap_if_reachable (s : state) (bot: bot) (c : coord) =
  if is_reachable s bot.coord c then
    set_wrapped s.map c

let pickup_powerups (s : state) (bot: bot) =
  let pows = s.powerups in
  match get_booster s.map bot.coord with
  | Some B ->
     pows.manipulators_collected <- pows.manipulators_collected + 1;
     remove_booster s.map bot.coord
  | Some F ->
     pows.fast_collected <- pows.fast_collected + 1;
     remove_booster s.map bot.coord
  | Some L ->
     pows.drill_collected <- pows.drill_collected + 1;
     remove_booster s.map bot.coord
  | Some X ->
     () (* Can't be picked up *)
  | Some R ->
     pows.teleports_collected <- pows.teleports_collected +1;
     remove_booster s.map bot.coord
  | Some C ->
     pows.clones_collected <- pows.clones_collected +1;
     remove_booster s.map bot.coord
  | None -> ()

let wrap_current_position (s: state) (bot: bot) =
  bot.arms |> List.iter ~f:(fun arm ->
    wrap_if_reachable s bot (move_coord bot.coord arm)
  )

let move_bot (s : state) (bot: bot) (c : coord) =
  match is_unblocked s.map c with
  | true -> 
      bot.coord <- c;
      wrap_current_position s bot;
  | false -> failwith ("Not allowed action: blocked position in time " ^ (Int.to_string s.clock))

(* May only be called when it's `bot`'s turn. Does not advance time. *)
let bot_step_unsafe (s: state) (bot: bot) (a: action) =
  begin
    match a with
    | W -> move_bot s bot (move_coord bot.coord (0,1))
    | S -> move_bot s bot (move_coord bot.coord (0,-1))
    | A -> move_bot s bot (move_coord bot.coord (-1,0))
    | D -> move_bot s bot (move_coord bot.coord (1,0))
    | Z -> ()
    | E -> 
        bot.dir <- rotate_direction_cw bot.dir;
        bot.arms <- bot.arms |> List.map ~f:rotate_dpos_cw;
        wrap_current_position s bot;
    | Q -> 
        bot.dir <- rotate_direction_ccw bot.dir;
        bot.arms <- bot.arms |> List.map ~f:rotate_dpos_ccw;
        wrap_current_position s bot;
    | B (dx,dy) ->
        if s.powerups.manipulators_collected < 1 then
          failwith "Tried to fit arms, but none are available";
        if bot.arms |> List.exists ~f:((=)(dx,dy))
        then failwith "arm already in that position"
        else if not (bot.arms |> List.map ~f:dneighbors |> List.concat |> List.exists ~f:((=)(dx,dy)))
        then failwith "arm not placed next to existing arms"
        else
          (* TODO: Generic line-of-sight? *)
          let in_broom = 
            match bot.dir with
            | Up ->    dy = 1
            | Down ->  dy = -1
            | Left ->  dx = -1
            | Right -> dx = 1 in
          if not in_broom then failwith "new arm not in broom formation";
          bot.arms <- (dx,dy) :: bot.arms;
          s.powerups.manipulators_collected <- s.powerups.manipulators_collected - 1
    | F -> failwith "Not implemented : fast wheels"
    | L -> failwith "Not implemented : drill"
    | R -> 
        if s.powerups.teleports_collected < 1 then
          failwith "No teleport beacons to drop";
        s.powerups.teleport_sites <- bot.coord :: s.powerups.teleport_sites;
        s.powerups.teleports_collected <- s.powerups.teleports_collected - 1
    | T pos ->
        if s.powerups.teleport_sites |> List.exists ~f:((=)pos)
        then move_bot s bot pos
        else failwith "this coordinate does not have a beacon"
    | C ->
        if s.powerups.clones_collected < 1 then
          failwith "No clones collected";
        s.powerups.clones_collected <- s.powerups.clones_collected - 1;
        let arms = [ (0,0); (1,0); (1,1); (1,-1) ] in
        let clone =
          { coord = bot.coord; dir = Right; arms; actions = Stack.create ();
            fast_active = 0; drill_active = 0; id=Stack.length s.bots }
        in
        Stack.push s.bots clone
  end;
  Stack.push bot.actions a

let step (s : state) (actions: bot -> action) =
  (* Turn the queue into a list in order to snapshot it since the actions
   * of bots may push clones on to the queue. *)
  Stack.to_list s.bots
  |> List.rev
  |> List.iter ~f:(fun bot ->
    bot_step_unsafe s bot (actions bot)
  );
  s.clock <- s.clock+1

type identity = {
  bot: bot
}
type 'a monad = identity -> 'a answer
and 'a answer =
  | Return of 'a
  | Action of action * 'a monad
  | Clone of unit monad * 'a monad

module Monad = Monad.Make(struct
  type 'a t = 'a monad

  let return x : 'a monad =
    fun _ -> Return x

  let rec bind (m: 'a t) ~(f: 'a -> 'b t) : 'b t =
    fun id ->
      match m id with
      | Return x -> f x id
      | Action (action, m') -> Action (action, bind m' ~f)
      | Clone (program, m') -> Clone (program, bind m' ~f)

  let map = `Define_using_bind (* TODO *)
end)

let return_unit = Monad.return () (* premature optimization *)

let action a : unit monad =
  fun _ -> Action (a, return_unit)

let clone program : unit monad =
  fun _ -> Clone (program, return_unit)

let get_bot : bot monad =
  fun { bot } -> Return bot

let run (s: state) (main: unit monad) : unit =
  let programs = Queue.singleton (Some main) in
  let rec step () =
    let bots_array = Stack.to_array s.bots in (* TODO: not great *)
    Array.rev_inplace bots_array; (* TODO: reverse *)
    assert (Queue.length programs = Array.length bots_array);
    let had_progress = ref false in
    Queue.to_list programs (* avoid mutation during iteration *)
    |> List.iteri ~f:(fun id program_opt ->
      let bot_progressed =
        match program_opt with
        | None -> false
        | Some program ->
            let bot = Array.get bots_array id in
            pickup_powerups s bot;
            match program { bot } with
            | Return () ->
                (* Ensure we'll never see this bot again. When we've skipped a
                * time step for a bot, we can never emit actions for that bot
                * again. *)
                Queue.set programs id None;
                false
            | Action (action, program) ->
                assert (action <> C);
                Queue.set programs id (Some program);
                bot_step_unsafe s bot action;
                true
            | Clone (clone_program, self_program) ->
                Queue.set programs id (Some self_program);
                Queue.enqueue programs (Some clone_program);
                bot_step_unsafe s bot C;
                true
      in
      had_progress := !had_progress || bot_progressed
    );
    if !had_progress then (
      s.clock <- s.clock + 1;
      step ()
    )
  in
  step ()

(*
let run ~(init: unit -> 'a) (s: state) (step: 'a -> bot -> 'a * action) =
  let max_bots = 1000 (* TODO *) in
  let bot_data = Option_array.create ~len:max_bots in
  Option_array.set_some bot_data 0 (init ());
  (* Turn the queue into a list in order to snapshot it since the actions
   * of bots may push clones on to the queue. *)
  Stack.to_list s.bots
  |> List.rev
  |> List.iteri ~f:(fun i bot ->
    let data = Option_array.get_some_exn bot_data i in
    let (data, action) = step data bot in
    bot_step_unsafe s bot action;
    Option_array.set_some bot_data i data;
  );
  (* TODO: if additional bots were created, make data for them *)
  (* TODO: this really ought to be a monadic interface! *)
  s.clock <- s.clock+1
*)

let unsafe_make_solution (s : state) : solution =
  Stack.to_list s.bots
  |> List.rev_map ~f:(fun bot ->
      List.rev (Stack.to_list (bot.actions))
  )

let validate (s : state) : solution =
  (* Printf.printf "Map:\n%s" (string_of_state s); *)
  let size = model_size s.map in
  for x = 0 to size do
    for y = 0 to size do
      let c = Coord (x,y) in 
      if should_be_wrapped s.map c
      then failwith "not a solution"
    done
  done;
  unsafe_make_solution s

(* Unit tests *)
let%test "diff_coor1" = diff_coor (Coord (3,2)) (Coord(1,-1)) = (-2,-3)
let%test "diff_coor2" = diff_coor (Coord (0,0)) (Coord(2,2)) = (2,2)
let%test "diff_coor3" = diff_coor (Coord (-5,-4)) (Coord(-3,-3)) = (2,1)

let%test "gcd1" = gcd 5 10 = 5
let%test "gcd2" = gcd 2 0 = 2
let%test "gcd3" = gcd 21 35 = 7
let%test "gcd4" = gcd 11 11 = 11
