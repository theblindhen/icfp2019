open Core
open Model

type component_map = {
    size: int;
    arr: int Array.t; (* color array *)
    conn_set: IntPair.Hash_set.t;
    max_id: int;
    component_ids: Int.Hash_set.t;
    components : (coord list * int) Int.Table.t;
  }

let empty_component_map (state : state) = 
  let r = model_size state.map in
  let conn_set = IntPair.Hash_set.create () in
  { size=r; arr=Array.create ~len:(r*r) (-1); conn_set; max_id=0;
    component_ids=Int.Hash_set.create ();
    components=Int.Table.create ();
  }

module Private = struct
  let c_coord_to_idx (cmap : component_map) (Coord (x,y) : coord) =
    x * cmap.size + y
   
  type uf_map = (coord Union_find.t) list * (coord Union_find.t) Coord.Table.t * CoordPair.Hash_set.t
    
  let uf_new_map () =
    let coord_tbl = Coord.Table.create () in
    let conn_set = CoordPair.Hash_set.create () in
    ([], coord_tbl, conn_set)

  let uf_set_of_coord ((_,coord_tbl,_) : uf_map) (coord : coord) =
    Hashtbl.find coord_tbl coord

  let uf_set_of_coord_exn ((_,coord_tbl,_) : uf_map) (coord : coord) =
    match Hashtbl.find coord_tbl coord with
      | None -> failwith "coord doesn't have a set"
      | Some s -> s

  let uf_coord_fresh_set ((_,coord_tbl,_) as uf : uf_map) (coord : coord) =
    match uf_set_of_coord uf coord with
      | None ->
         let set = Union_find.create coord in
         if Hashtbl.add coord_tbl ~key:coord ~data:set <> `Ok then
           failwith "It wasn't fresh?";
         set
      | Some _ -> failwith "Coord already has a set"

  let uf_has_set ((_,coord_tbl,_) : uf_map) (coord : coord) =
    match Hashtbl.find coord_tbl coord with
      | None -> false
      | Some _ -> true

  let uf_set_of_coord_or_fresh (uf : uf_map) (coord : coord) =
    match uf_set_of_coord uf coord with
      | None -> uf_coord_fresh_set uf coord
      | Some s -> s

  let uf_coord_to_int (_ : uf_map) (Coord (x,y) : coord) =
    (* NOTE: Doesn't work for ridiculously large maps *)
    x * 100000 + y

  let uf_coord_color_exn (uf : uf_map) (c : coord) =
    c |> uf_set_of_coord_exn uf |> Union_find.get |> (uf_coord_to_int uf)

  let uf_coord_color (uf : uf_map) (c : coord) =
    match uf_set_of_coord uf c with
      | None -> None
      | Some set -> Some (set |> Union_find.get |> (uf_coord_to_int uf))

  let uf_add_connection ((_,_,conn_set) as uf : uf_map) (c1 : coord) (c2 : coord) =
    assert (Union_find.get (uf_set_of_coord_exn uf c1) <> Union_find.get (uf_set_of_coord_exn uf c2));
    Hash_set.add conn_set (c1,c2)

  (* Add a component consisting of a list of coords. Merges with any existing
   * component containing one of those coords *)
  let uf_add_component_from_coords (uf : uf_map) (coords : coord list) =
    match coords with
    | [] -> ()
    | hd::coords ->
       let set = uf_set_of_coord_or_fresh uf hd in
       List.iter coords ~f:(fun c -> Union_find.union set (uf_set_of_coord_or_fresh uf c))

  let uf_grown_from_points (state : state) (uf : uf_map) (coords : coord list) =
    let visited = Coord.Hash_set.create () in
    let next = ref [] in
    coords |> List.filter ~f:(is_unblocked state.map)
           |> List.iter ~f:(fun c ->
                  uf_coord_fresh_set uf c |> ignore;
                  Hash_set.add visited c;
                  next:= c :: !next);
    let rec flood () =
      let curr = !next in
      next := [];
      List.iter curr ~f:(fun c ->
          let c_set = uf_set_of_coord_exn uf c in
          neighbors c
          |> List.filter ~f:(is_unblocked state.map)
          |> List.iter ~f:(fun n ->
                 match uf_set_of_coord uf n with
                 | None ->
                    let n_set = uf_coord_fresh_set uf n in
                    Union_find.union c_set n_set;
                    Hash_set.add visited n;
                    next := n :: !next;
                 | Some n_set ->
                    if Union_find.get c_set <> Union_find.get n_set then
                      uf_add_connection uf c n;
               ));
      match !next with
      | [] -> ()
      | _  -> flood ()
    in
    flood ()

  let get_color (cmap : component_map) (c : coord) =
    Array.get cmap.arr (c_coord_to_idx cmap c)

  let set_color (cmap : component_map) (c : coord) (color : int) =
    Hashtbl.update cmap.components color ~f:(function
    | None ->
       Hash_set.add cmap.component_ids color;
       ([c], 1)
    | Some ((ls, w)) -> (c::ls, w+1));
    Array.set cmap.arr (c_coord_to_idx cmap c) color

  let add_connection (cmap : component_map) (color1 : int) (color2 : int) =
    if color1 = color2 then
      ()
    else begin
      Hash_set.add cmap.conn_set (color1,color2);
      Hash_set.add cmap.conn_set (color2,color1)
    end

  let component_map_of_uf (state : state) (uf : uf_map) =
    let cmap = empty_component_map state in
    let max_id = ref 0 in
    iter_over_unblocked state (fun coord ->
        match uf_coord_color uf coord with
          | None -> (* TODO: Should never happen *)
             set_color cmap coord (-2)
          | Some color ->
             if color > !max_id then
               max_id := color;
             set_color cmap coord color;
      );
    let (_,_,uf_conn_set) = uf in
    Hash_set.iter uf_conn_set ~f:(fun (coord1, coord2) ->
        add_connection cmap (get_color cmap coord1) (get_color cmap coord2));
    { cmap with max_id = !max_id }

end
open Private
  
let get_color = Private.get_color

let component_map_grow_from_coords (state : state) (coords : coord list) =
  let uf = uf_new_map () in
  uf_grown_from_points state uf coords;
  component_map_of_uf state uf

let component_map_grow_from_random_coords (state : state) (random_comps : int) =
  let r = model_size state.map in
  let coords =
    List.init random_comps ~f:(fun _ -> find_random_coord r (is_unblocked state.map))
    |> Coord.Set.of_list
    |> Set.to_list
  in
  component_map_grow_from_coords state coords

let component_map_init_and_random_fill (state : state) (init_comps : coord list list) (random_comps : int) =
  let uf = uf_new_map () in
  List.iter init_comps ~f:(uf_add_component_from_coords uf);
  List.init random_comps ~f:(fun _ -> find_random_coord (model_size state.map) (is_unblocked state.map))
   |> Coord.Set.of_list
   |> Set.to_list
   |> List.filter ~f:(fun coord -> not (uf_has_set uf coord))
   |> uf_grown_from_points state uf;
  (* Go through every remaining position and fill to make sure everything was covered *)
  iter_over_unblocked state (fun coord ->
      match uf_set_of_coord uf coord with
      | None -> uf_grown_from_points state uf [coord]
      | Some _ -> ());
  component_map_of_uf state uf

  
  



let component_coords_exn (cmap : component_map) (color : int) =
  let (comps,_) = Hashtbl.find_exn cmap.components color in
  comps

let is_connected (cmap : component_map) (color1 : int) (color2 : int) =
  Hash_set.mem cmap.conn_set (color1, color2)

let same_component (cmap : component_map) (c1 : coord) (c2 : coord) =
  get_color cmap c1 = get_color cmap c2

let neighbor_components (cmap : component_map) (color : int) =
  Hash_set.filter cmap.conn_set ~f:(fun (c1,c2) -> color = c1 && color <> c2)
    |> Hash_set.to_list
    |> List.map ~f:(fun (_,c2) -> c2)

let component_size (cmap : component_map) (color : int) =
  let (_, size) = Hashtbl.find_exn cmap.components color in
  size

let count_components (cmap : component_map) =
  Hash_set.length cmap.component_ids

let color_to_char (color : int) =
  match color with
    | -1 -> 'W'
    | -2 -> '$'
    | _ ->
       match Char.of_int (63 + ((color * 12367) % 64)) with
       | None -> '@'
       | Some c -> c

let string_of_component_map (state : state) (cmap : component_map) =
  let endlines = "*"^ (String.init cmap.size ~f:(fun _ -> '-')) ^"*" in
  let body = 
    List.init cmap.size ~f:(fun negy ->
        let y = cmap.size - negy - 1 in
        let body = 
            List.init cmap.size ~f:(fun x ->
                let coord = Coord (x,y) in
                if is_blocked state.map coord then
                  " "
                else
                  get_color cmap coord |> color_to_char |> Char.to_string)
            |> String.concat in
        "|"^ body ^"|"
        )
    |> String.concat ~sep:"\n" in
  let ncomps = Printf.sprintf "Number of components: %d" (count_components cmap) in
  endlines ^"\n"^ body ^"\n"^ endlines ^"\n"^ ncomps




let%test "union_find" =
  Union_find.same_class (Union_find.create 2) (Union_find.create 2) = false
  
