open Core

module IntPair = struct
  module T = struct
    type t = int * int [@@deriving compare, hash, sexp]
  end
  include T
  include Hashable.Make (T)
  include Comparable.Make (T)
end

let hello_world_string =
  let tbl = IntPair.Table.create () in
  let map = IntPair.Map.singleton (0, 2) "Hello, World" in
  Hashtbl.set tbl ~key:(0, 1) ~data:map;
  let open Option in
  Hashtbl.find tbl (0, 1)
  >>= Fn.flip Map.find (0, 2)
  |> Option.value ~default:"!"

let%test "starts with Hello" =
  String.is_prefix hello_world_string ~prefix:"Hello"

let%test "associativity of list append" =
  Quickcheck.test Quickcheck.Generator.(tuple3
      (list small_non_negative_int)
      (list small_non_negative_int)
      (list small_non_negative_int)
    )
    ~sexp_of:[%sexp_of: int list * int list * int list]
    ~f:(fun (xs, ys, zs) ->
      assert (
        (List.append xs (List.append ys zs)) =
        (List.append (List.append xs ys) zs))
    );
    true
