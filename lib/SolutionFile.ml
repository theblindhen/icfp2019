open Core

type coord = Coord of int * int
  [@@deriving compare, hash, sexp]

(* Boilerplate to let us put `coord` in hash tables *)
module Coord = struct
  module T = struct
    type t = coord
    let hash = hash_coord
    let sexp_of_t = sexp_of_coord
    let t_of_sexp = coord_of_sexp
    let compare = compare_coord
    let hash_fold_t = hash_fold_coord
  end
  include T
  include Hashable.Make(T)
  include Comparable.Make (T)
end

(* Boilerplate to let us put `coord pair` in hash tables *)
module CoordPair = struct
  module T = struct
    type t = Coord.t * Coord.t
      [@@deriving compare, hash, sexp]
  end
  include T
  include Hashable.Make(T)
  include Comparable.Make (T)
end

type action =
  | W (** move up *)
  | S (** move down *)
  | A (** move left *)
  | D (** move right *)
  | Z (** do nothing *)
  | E (** clockwise *)
  | Q (** counterclockwise *)
  | B of int * int (** attach manipulator at (dx, dy) *)
  | F (** attach fast wheels *)
  | L (** start using a drill *)
  | R (** reset a teleport, i.e. install a beacon **)
  | T of coord (** goto the teleport at this coord **)
  | C [@@deriving compare, hash, sexp]

type actions = action list

type solution = actions list

let string_of_action = function
  | W -> "W"
  | S -> "S"
  | A -> "A"
  | D -> "D"
  | Z -> "Z"
  | E -> "E"
  | Q -> "Q"
  | B (dx, dy) -> Printf.sprintf "B(%d,%d)" dx dy
  | F -> "F"
  | L -> "L"
  | R -> "R"
  | T (Coord (x,y)) -> Printf.sprintf "T(%d,%d)" x y
  | C -> "C"

let string_of_actions (actions: actions) =
  String.concat (List.map ~f:string_of_action actions)

let string_of_solution (solution: solution) =
  String.concat ~sep:"#" (List.map ~f:string_of_actions solution) ^ "\n"
