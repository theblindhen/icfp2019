open Core
open SolutionFile
open Model
open SolverUtil
open Components

type environment = {
    state : state;
    cmap : component_map;
    assignments : int Int.Table.t;  (* botid -> comp *)
    rectangles : (int * rectangle) list;  (* list of rectangles and their component colors *)
    unclaimed: Int.Hash_set.t;     (* currently unassigned comps *)
    completed: Int.Hash_set.t;    (* completed comps *)
    mutable ignore_components: bool ;     (* Flag to disable components *)
  }

let astar_in_components (env : environment) (start_comp : int) (heuristic : (int * int list * int) -> int) (needle : int -> bool) =
  let visited = Int.Hash_set.create () in
  let queue = Heap.create ~cmp:(fun a1 a2  -> Int.compare (heuristic a1) (heuristic a2)) ()  in
  (* `queue` contains only passable coordinates *)
  Hash_set.add visited start_comp;
  Heap.add queue (start_comp, [], 0); (* current, path, total weight *)
  let rec search () =
    match Heap.pop queue with
    | None -> None (*  *)
    | Some (comp, path_rev, d) ->
        match needle comp with
        | true -> Some comp (* found nearest needle *)
        | false ->
            (* Add every new neighbor to `visited` and `queue` *)
           neighbor_components env.cmap comp
           |> List.filter ~f:(fun c -> not (Hash_set.mem visited c))
           |> List.iter ~f:(fun comp ->
                  Hash_set.add visited comp;
                  Heap.add queue (comp, (comp :: path_rev), d + component_size env.cmap comp)
                );
           search ()
  in
  search ()

let get_near_component (env : environment) (start_comp : int) (needle : int -> bool) =
  let res = astar_in_components env start_comp (fun (_cur, _path_rev, weight) -> weight) needle in
  res

let try_rotate (direction: [`e|`q]) map arms coord =
  let rotation =
    match direction with
    | `e -> rotate_dpos_cw
    | `q -> rotate_dpos_ccw
  in
  let arms = List.map arms ~f:rotation in
  let new_wrapped_count =
    List.count arms ~f:(fun arm ->
      let coord = Model.move_coord coord arm in
      is_unblocked map coord &&
      not (is_wrapped map coord) (* TODO: line of sight *)
    )
  in
  (arms, new_wrapped_count)

let find_eager_rotation map arms coord =
  let arms_e, new_e = try_rotate `e map arms coord in
  let _arms_q, new_q = try_rotate `q map arms coord in
  if new_e > 0
  then
    if new_e > new_q
    then Some E
    else Some Q
  else
    (* We don't apply the wrapping effect of the previous rotation before
      * rotating again. That's okay because there wasn't any effect since
      * `new_e` was 0. *)
    let _arms_ee, new_ee = try_rotate `e map arms_e coord in
    (* If it's a good idea to turn E twice, we do it just once. We'll
      * discover in the next step that we should turn again. *)
    if new_ee > 0
    then Some E
    else None

(* Returns whether it's unblocked and not wrapped in the direction where
 * `direction` takes us. *)
let is_clear_in_front_of =
  let deltasD = [1,-1; 1,0; 1,1] in
  let deltasW = List.map ~f:rotate_dpos_ccw deltasD in
  let deltasA = List.map ~f:rotate_dpos_ccw deltasW in
  let deltasS = List.map ~f:rotate_dpos_ccw deltasA in
  fun map coord direction ->
    let deltas =
      match direction with
      | A -> deltasA
      | W -> deltasW
      | D -> deltasD
      | S -> deltasS
      | _ -> failwith "is_clear_in_front_of: not a move"
    in
    List.for_all deltas ~f:(fun delta ->
      let coord = move_coord coord delta in
      is_unblocked map coord &&
      not (is_wrapped map coord)
    )

let get_side_moves map coord direction =
  let deltas =
    match direction with
    | W | S -> [A, (-1,0) ; D, (1,0)]
    | A | D -> [S, (0,-1) ; W, (0,1)]
    | _ -> failwith "get_side_moves: not a direction"
  in
  deltas
  |> List.map ~f:(fun (action, delta) -> (action, move_coord coord delta))
  |> List.filter ~f:(fun (_, coord) -> is_unblocked map coord)

let list_max_elements ~compare =
  let rec keep_better_than ~first_best ~other_best ~tail =
    match tail with
    | [] -> first_best :: other_best
    | h::t ->
        let compared = compare first_best h in
        if compared = 0 (* equivalent *)
        then keep_better_than ~first_best ~other_best:(h :: other_best) ~tail:t
        else
          if compared > 0 (* first_best > h *)
          then keep_better_than ~first_best ~other_best ~tail:t
          else (* first_best < h *)
            keep_better_than ~first_best:h ~other_best:[] ~tail:t
  in
  function
  | [] -> []
  | x :: xs -> keep_better_than ~first_best:x ~other_best:[] ~tail:xs

let where_to_go env bot color =
  let map = env.state.map in
  (* Rate how attractive it is to wrap this coordinate. *)
  let rate_coord map ~bot_coord coord =
    match
      is_blocked map coord ||
      not (Sim.is_reachable env.state bot_coord coord)
    with
    | true -> -1
    | false ->
        match is_wrapped map coord with
        | true -> -1
        | false ->
            List.sum (module Int) (neighbors coord) ~f:(fun coord ->
              match is_blocked map coord with
              | true -> 2
              | false ->
                  match is_wrapped map coord with
                  | true -> 3
                  | false -> 0
            )
  in
  (* Rate how attractive it is to put the bot on this coordinate. *)
  let rate_bot_placement map coord =
    List.sum (module Int) bot.arms ~f:(fun arm ->
      rate_coord map ~bot_coord:coord (move_coord coord arm)
    )
  in
  neighbors_with_action bot.coord
  |> List.filter ~f:(fun (_, coord) ->
      is_unblocked map coord &&
      (env.ignore_components || color = get_color env.cmap coord)
    )
  |> List.map ~f:(fun (action, coord) ->
      (action, coord, rate_bot_placement map coord)
    )
  |> List.filter ~f:(fun (_, _, rating) -> rating >= 0)
  |> list_max_elements ~compare:(fun (_, _, a) (_, _, b) -> Int.compare a b)
  |> List.map ~f:(fun (action, coord, _) -> ([action], coord))


let bot_proximity state coord =
  (* Note: this sum includes the bot itself, which always contributes a term of
   * 1.0. It's more convenient to just have this term than to filter it out. *)
  Stack.sum (module Float) state.bots ~f:(fun bot ->
    let dist = manhattan_distance bot.coord coord in
    1. /. (1. +. Float.of_int dist)
  )

let component_finished env color =
  component_coords_exn env.cmap color 
  |> List.exists ~f:(should_be_wrapped env.state.map)
  |> not

let plan_actions_rev actions_rev =
  Sim.Monad.all_unit (List.rev_map ~f:Sim.action actions_rev)

let bot_assignment (env: environment) (bot : bot) =
  Hashtbl.find env.assignments bot.id

let new_assignment (env: environment) (bot : bot) =
  (match bot_assignment env bot with
  | Some comp ->
     assert (component_finished env comp);
     Hashtbl.remove env.assignments bot.id;
     Hash_set.add env.completed comp;
  | None -> ());
  let compo =
    let cur_comp = get_color env.cmap bot.coord in
    match Hash_set.is_empty env.unclaimed with
    | false ->
        (* Find a close unclaimed *)
        get_near_component env cur_comp (Hash_set.mem env.unclaimed)
    | true ->
       begin
         let assigned = Hashtbl.to_alist env.assignments
                        |> List.map ~f:(fun (_botid, comp) -> comp)
                        |> List.filter ~f:(fun comp ->
                               not (Hash_set.mem env.completed comp))
         in
       match assigned with
       | []-> 
          None
          (* Nothing left *)
       | _ ->
          let assigned_s = Int.Set.of_list assigned in
          (* Find a close claimed *)
          get_near_component env cur_comp (Set.mem assigned_s)
       end in
  match compo with
  | Some comp ->
     Hash_set.remove env.unclaimed comp;
     if Hashtbl.add env.assignments ~key:bot.id ~data:comp <> `Ok then
       failwith "This should have been removed 2";
     Some comp
  | None -> None

let bot_assignment_or_get_new (env: environment) (bot : bot) =
  match bot_assignment env bot with
  | Some c -> Some c
  | None -> new_assignment env bot

let find_my_unwrapped (env : environment) (start_coord : coord) (color : int) =
  let map = env.state.map in
  match component_finished env color with
  | true -> []
  | false ->
     let visited = Bitv.create (model_size map * model_size map) false in
     let needle coord =
       should_be_wrapped map coord
         && (env.ignore_components || color = get_color env.cmap coord) in
     (* Invariant: `next` contains only unblocked coordinates *)
     let next = ref [] in
     Bitv.set visited (coord_to_idx map start_coord) true;
     let rec search current =
       let results =
         List.filter current ~f:(fun (actions_rev, coord) ->
             match needle coord with
             | true -> true (* found a nearest unwrapped *)
             | false ->
                (* Add every new neighbor to `visited` and `queue` *)
                neighbors_with_action coord
                |> List.filter ~f:(fun (_, coord) ->
                       is_unblocked map coord &&
                         not (Bitv.get visited (coord_to_idx map coord))
                     )
                |> List.iter ~f:(fun (action, coord) ->
                       (* It's impotant to add it to `visited` already here, even
                        * though we haven't strictly "visited" it. Otherwise we risk
                        * adding it to the queue multiple times. *)
                       Bitv.set visited (coord_to_idx map coord) true;
                       next := ((action :: actions_rev), coord) :: !next
                     );
                false
           )
       in
       match results with
       | _::_ -> results
       | [] ->
          begin match !next with
          | [] -> failwith "But there's still unwrapped component coords left!"
          | current ->
             next := [];
             search current
          end
     in
     search [([], start_coord)]

let rec slave_logic (env: environment) =
  let open Sim.Monad in
  Sim.get_bot >>= fun bot ->
  match bot_assignment_or_get_new env bot with
  | None ->
     return ()
  | Some bot_comp ->
    (* TODO: Don't go to the nearest field if we can go one distance
    * further and wrap everything in between thanks to our arms. *)
    (* TODO: As a tie breaker between equally good neighbors, continue in
    * the direction we're going? *)
    match
        let plan_a = where_to_go env bot bot_comp in
        let plan =
        match plan_a with
        | [] -> find_my_unwrapped env bot.coord bot_comp
        | _ -> plan_a
        in
        plan
        (* tie breaker: move away from other bots *)
        |> List.min_elt ~compare:(fun (_, coord1) (_, coord2) ->
            Float.compare
            (bot_proximity env.state coord1)
            (bot_proximity env.state coord2)
            )
    with
    | None ->
       new_assignment env bot |> ignore;
       slave_logic env (* Get a new job *)
    | Some (actions_rev, final_coord) ->
        let actions_rev =
        match actions_rev with
        | last_move::other_moves ->
            let rotations_rev =
                match bot.dir, last_move with
                | Up, W | Right, D | Down, S | Left, A -> []
                | Up, D | Right, S | Down, A | Left, W -> [E]
                | Up, A | Right, W | Down, D | Left, S -> [Q]
                | Up, S | Right, A | Down, W | Left, D -> [E; E]
                | _, (Z | E | Q | B (_, _) | F | L | R | T _ | C) ->
                failwith "Action wasn't a move"
            in
            if is_clear_in_front_of env.state.map final_coord last_move
            then last_move :: rotations_rev @ other_moves
            else
                let side_moves =
                get_side_moves env.state.map final_coord last_move
                in
                begin match
                List.find side_moves ~f:(fun (_action, coord) ->
                    is_clear_in_front_of env.state.map coord last_move
                    )
                with
                | Some (action, _coord) ->
                action :: last_move :: rotations_rev @ other_moves
                | None -> actions_rev
                end
        | _ -> actions_rev
        in
        plan_actions_rev actions_rev >>= fun () ->
        slave_logic env

let rec master_logic (env: environment) =
  let open Sim.Monad in
  Sim.get_bot >>= fun bot ->
  if env.state.powerups.manipulators_collected > 0 then
    (* Immediately use arms when we have them *)
    Sim.action (bot_extend_broom env.state bot) >>= fun () ->
    master_logic env
  else if env.state.powerups.clones_collected > 0 && get_booster env.state.map bot.coord = Some X then
    (* Then clone whenever we can *)
    Sim.clone (slave_logic env) >>= fun () ->
    master_logic env
  else
    (* Then pick up all boosters on the map and goto X if we have clones *)
    let policy = [ ([C;B], 10) ; ([B;C;X], Int.max_value) ] in
    match find_nearest_booster env.state bot policy with
    | Some (_, actions_rev) ->
       (* TODO: Postprocess actions to conservatively insert rotations *)
       plan_actions_rev actions_rev >>= fun () ->
       master_logic env
    | None ->
       (* Revert to being a slave *)
       slave_logic env

let make_component_map state ignore_rects =
  let component_count =
    let size = model_size state.map in
    if size < 50 then 5
    else if size < 100 then 10
    else if size < 200 then 20
    else if size < 400 then 40
    else 64
  in
  let rect_min_size = 64 in
  let rect_fill_area = (model_size state.map) * (model_size state.map) * 3 / 10 in
  let rec rects_till_area (rects : rectangle list) min_area =
    match rects with
    | [] -> []
    | r :: rects ->
        r :: (
          if r.width * r.height > min_area
          then []
          else rects_till_area rects (min_area - r.width * r.height)) in
  let rects =
    if ignore_rects
    then []
    else 
      let rects_large_enough = RectFinder.find_largest_unblocked_rects state.map rect_min_size in
      rects_till_area rects_large_enough rect_fill_area in
  let rect_coords = List.map rects ~f:Model.coords_of_rect in
  let cmap = component_map_init_and_random_fill state rect_coords component_count in
  let rmap = List.map rects ~f:(fun r -> (get_color cmap r.llc, r)) in
  (rmap, cmap)

let solve ~(ignore_components : bool) ~(ignore_rects : bool) (state : state) : solution =
  let (rmap, cmap) = make_component_map state ignore_rects in
  let env = {
      state = state;
      cmap = cmap;
      assignments = Int.Table.create ();
      rectangles = rmap;
      unclaimed = Int.Hash_set.create ();
      completed = Int.Hash_set.create ();
      ignore_components = ignore_components;
    } in
  (* TODO: Filter out rectangle component ids in theh following *)
  env.cmap.component_ids
  |> Hash_set.to_list
  |> List.iter ~f:(Hash_set.add env.unclaimed);
  (* Printf.printf "\nComponent Map:\n%s\n\n" (Components.string_of_component_map state env.cmap);
   * Hash_set.iter env.cmap.component_ids ~f:(fun color -> Printf.printf "Size of %d: %d\n" color (component_size env.cmap color)); *)
  let () = Sim.run state (master_logic env) in
  Sim.validate state
