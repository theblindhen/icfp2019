open Core
open Model

type broom = {width : int; size_left : int; size_right : int }

(* Returns the size of the broom as (width,left,right) *)
let bot_broom bot =
  let broom_width = List.length bot.arms - 1 in
  {width = broom_width;  size_left = broom_width/2; size_right = (broom_width-1)/2}

let bot_extend_broom state bot =
  if state.powerups.manipulators_collected = 0 
  then failwith "not enough manipulators"
  else
    (* Attach in broom formation *)
    let broom = bot_broom bot in
    let (ux,uy) = 
      if broom.width % 2 = 0 then
        (* Attach right *)
        (1, -(broom.size_right+1))
      else
        (* Attach left *)
        (1, broom.size_left+1) in
    let (dx, dy) = rotate_dpos_from_right_to_dir (ux,uy) bot.dir in
    SolutionFile.B (dx,dy) 

let find_nearest_booster (state: state) (bot : bot) (policy: (booster list * int) list)  =
  match boosters_left state.map with
  | [] -> None
  | boosters_left ->
     let find_powerup (boosters, radius) =
       let is_sought_booster (booster,coord) = coord <> bot.coord && List.exists boosters ~f:((=) booster) in
       let is_sought_booster' coord =
         match get_booster state.map coord with
           | Some c -> is_sought_booster (c, coord)
           | None -> false in
       if not (any_satisfactory_boosters_left boosters_left is_sought_booster) then (* Fail fast *)
         None
       else
         match find_near_manhattan bot.coord radius (is_unblocked state.map) is_sought_booster' with
         | Some (final_coord, actions, _d) -> Some (final_coord, List.rev actions)
         | None -> None
     in
     let policy = if state.powerups.clones_collected > 0 then policy
                  else List.map policy ~f:(fun (bs, r) -> (List.filter bs ~f:((<>) X), r))
     in
     policy
     |> List.fold ~init:None ~f:(fun osol round_policy ->
            match osol with
            | Some _ -> osol
            | None -> find_powerup round_policy)
