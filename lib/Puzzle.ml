open Core
open Model
open TaskFile
open TaskFile.Private

exception PuzzleFailed of string
   
type puzzle = {
    block_num: int;
    epoch : int;
    max_size: int;
    vertex_min: int;
    vertex_max: int;
    manipulators: int;
    fasts: int;
    drills: int;
    teleports: int;
    clones: int;
    spawn_points: int;
    include_coords: coord list;
    exclude_coords: coord list;
    }

type puzzle_map = PMap of int * Bitv.t * Coord.Hash_set.t * Coord.Hash_set.t (* size, map of solution, set of includes, set of excludes *)

type solution = coord list * coord * (Model.booster * coord) list  (* Contour map , bot , boosts *)

let new_puzzle_map (puzzle : puzzle) =
  let map = Bitv.create (puzzle.max_size*puzzle.max_size) false in
  let include_set = Coord.Hash_set.create() in
  let exclude_set = Coord.Hash_set.create() in
  List.iter puzzle.include_coords ~f:(Hash_set.add include_set);
  List.iter puzzle.exclude_coords ~f:(Hash_set.add exclude_set);
  PMap (puzzle.max_size, map, include_set, exclude_set)

let coord_to_idx (PMap (s,_,_,_) : puzzle_map) (Coord (x,y) : coord) =   x * s + y

let idx_to_coord (PMap (s,_,_,_) : puzzle_map) (idx : int) =
  Coord(idx/s, idx % s)

let puzzle_size (PMap (s,_,_,_) : puzzle_map) = s

let is_within_bounding_box (p_map: puzzle_map) (Coord (x,y): coord) =
  x >= 0 && y >= 0 && x < (puzzle_size p_map) && y < (puzzle_size p_map)

let is_excluded (p_map : puzzle_map) (c : coord) =
  let PMap (_,_,_,excludes) = p_map in
  not (is_within_bounding_box p_map c) ||
  Hash_set.mem excludes c

let is_allowed p_map c = not (is_excluded p_map c)

let is_included (p_map : puzzle_map) (c : coord) =
  let PMap (_,_,includes,_) = p_map in
  Hash_set.mem includes c

let mark (PMap (_,map,_,_) as p_map : puzzle_map) (c : coord) =
  Bitv.set map (coord_to_idx p_map c) true

let is_marked (p_map : puzzle_map) (c : coord) =
  let PMap (_,map,_,_) = p_map in
  is_within_bounding_box p_map c &&
  Bitv.get map (coord_to_idx p_map c)

let all_marked (PMap (_,map,_,_) as pmap : puzzle_map) : coord list =
  Bitv.foldi_left (fun acc idx marked ->
      if marked then
        (idx_to_coord pmap idx)::acc
      else
        acc) [] map

let puzzlemap_to_string (pmap : puzzle_map) =
  let PMap (r, _, _, _) = pmap in
  let endlines = "*"^ (String.init r ~f:(fun _ -> '-')) ^"*" in
  let body = 
    List.init r ~f:(fun negy ->
        let y = r - negy - 1 in
        let body = 
            List.init r ~f:(fun x ->
                let pos = Coord (x,y) in
                if is_included pmap pos && is_marked pmap pos then
                    "I"
                else if is_included pmap pos && not (is_marked pmap pos) then
                    "Y"
                else if is_excluded pmap pos && not (is_marked pmap pos) then
                    "O"
                else if is_excluded pmap pos && is_marked pmap pos then
                    "&"
                else if not (is_marked pmap pos) then
                    "#"
                else
                    " "
                )
            |> String.concat in
        "|"^ body ^"|"
        )
    |> String.concat ~sep:"\n"
  in
  endlines ^"\n"^ body ^"\n"^ endlines ^"\n"

(* Connects the includes by going up, right, down, left in that priority *)
let greedy_path (PMap (_,_,includes,_) as p_map : puzzle_map) =
  let sorted_nodes = 
    Hash_set.to_list includes
    |> List.sort ~compare:(fun c1 c2 -> Int.compare (coord_to_idx p_map c1) (coord_to_idx p_map c2)) in
  let rec move (Coord (x,y)) (Coord (xt,yt) as target) =
    (* Printf.printf "\nMap:\n%s" (puzzlemap_to_string p_map); *)
    let (up, right, down, left) = (Coord (x,y+1), Coord (x+1,y), Coord (x,y-1), Coord (x-1,y)) in
    let next = 
      match Int.compare x xt, Int.compare y yt with
      | 0, 0 -> None
      | xc,yc when yc = 0 && xc < 0 && is_allowed p_map right -> Some right
      | xc,yc when yc = 0 && xc < 0 && is_allowed p_map up -> Some up
      | _, yc when yc < 0 && is_allowed p_map up -> Some up
      | xc,yc when yc < 0 && xc < 0 && is_allowed p_map right -> Some right
      | xc,yc when yc < 0 && xc > 0 && is_allowed p_map left -> Some left
      | _, yc when yc < 0 && is_allowed p_map right -> Some right
      | _, yc when yc < 0 && is_allowed p_map left -> Some left
      | _, yc when yc > 0 && is_allowed p_map down -> Some down
      | xc,yc when yc > 0 && xc < 0 && is_allowed p_map right -> Some right
      | xc,yc when yc > 0 && xc > 0 && is_allowed p_map left -> Some left
      | _, yc when yc > 0 && is_allowed p_map right -> Some right
      | _, yc when yc > 0 && is_allowed p_map left -> Some right
      | _, _ -> raise (PuzzleFailed "Nowhere to go") in
    match next with
    | None -> ()
    | Some next_coord -> 
        mark p_map next_coord;
        move next_coord target in 
  let rec connect curr = 
    function 
    | [] -> () (* Done *)
    | next :: tail -> 
        move curr next;
        connect next tail in
  match sorted_nodes with
  | [] -> () (* No includes *)
  | start :: path -> 
      mark p_map start;
      connect start path  

let partition (coords : coord list) is_fillable =
  let visited = Coord.Hash_set.create() in
  let rec flood (cs : coord list) (part : coord list) = 
    match cs with
    | [] -> part
    | _ -> 
        let ns = 
          cs 
          |> List.map ~f:neighbors 
          |> List.concat 
          |> Coord.Set.of_list
          |> Set.to_list
          |> List.filter ~f:(fun c -> List.mem coords ~equal:(=) c)
          |> List.filter ~f:(fun c -> not(Hash_set.mem visited c))
          |> List.filter ~f:is_fillable in
        List.iter ns ~f:(fun n -> Hash_set.add visited n);
        flood ns (List.append cs part) in
  let rec iter = function
    | [] -> []
    | c :: cs ->
        match not(Hash_set.mem visited c) && is_fillable c with
        | false -> iter cs
        | true ->
            Hash_set.add visited c; 
            let part = flood [c] [] in
            part :: (iter cs) in
  iter coords

let is_safe_to_mark (p_map : puzzle_map) (Coord (x,y) as coord : coord) =
  let is_fillable = fun c -> is_allowed p_map c && not (is_marked p_map c) in
  if is_fillable coord then
    let ns = 
        [
        Coord (x-1,y+1); Coord (x,y+1); Coord (x+1,y+1);
        Coord (x-1,y); Coord (x+1,y);
        Coord (x-1,y-1); Coord (x,y-1); Coord (x+1,y-1);
        ] in
    let parts_before = partition (coord :: ns) is_fillable in
    let parts_after = partition ns is_fillable in
    List.length parts_before >= List.length parts_after
  else
    false

(* Connects the includes by going up, right, down, left in that priority *)
let spider_markable_path (PMap (size, _,includes,_) as p_map : puzzle_map) =
  let sorted_nodes = 
    Hash_set.to_list includes
    |> List.sort ~compare:(fun c1 c2 -> Int.compare (coord_to_idx p_map c1) (coord_to_idx p_map c2)) in
  let path_to_node (cs : coord list) start =
    let passable = is_allowed p_map in
    let needle = List.mem ~equal:(=) cs in
    match find_near_manhattan start size passable needle with
    | None -> raise (PuzzleFailed "no path to node found")
    | Some (_, actions, _) -> coord_list_of_actions start actions in
  let rec connect = 
    function 
    | [] -> () (* Done *)
    | next :: tail -> 
       let path = next :: path_to_node (all_marked p_map) next in
       List.iter path ~f:(mark p_map);
       connect tail in
  match sorted_nodes with
    | [] -> ()
    | hd::sorted_nodes ->
       mark p_map hd;
       connect sorted_nodes  

let grow_appendages (pmap : puzzle_map) ~(growths: int) ~(girth: int) =
  let i = ref 0 in
  while !i < growths do
    let PMap (size,_,_,_) = pmap in
    let coord = find_random_coord size (is_marked pmap) in
    let weight = ref 0 in
    let rec grow (c : coord) =
      neighbors c
      |> List.iter ~f:(fun nc -> 
             if !weight < girth && not (is_marked pmap nc) && is_safe_to_mark pmap nc then begin
                 mark pmap nc;
                 weight := !weight + 1;
                 grow nc
               end);
    in
    grow coord;
    if !weight > 0 then
      i := !i + 1
  done

let count_marked (pmap : puzzle_map) = 
  let PMap (_,map,_,_) = pmap in
  map
  |> Bitv.fold_left (fun sum is_marked ->
         match is_marked with
         | false -> sum
         | true -> sum+1) 0
 
let validate_puzzlemap (puzzle: puzzle) (pmap: puzzle_map) =
  let PMap (size,map,_,_) = pmap in
  (* Contains all iSqs *)
  if List.exists puzzle.include_coords ~f:(fun c -> not (is_marked pmap c)) then
    raise (PuzzleFailed (Printf.sprintf "Invalid Puzzlemap: included square not marked"));
  (* Does not contain any oSqs *)
  if List.exists puzzle.exclude_coords ~f:(fun c -> is_marked pmap c) then
    raise (PuzzleFailed "Invalid Puzzlemap: excluded square marked");
  (* No obstacles *)
  let () =
    let check_map = Bitv.copy map in
    let some_empty =
        match puzzle.exclude_coords with
        | [] -> raise (PuzzleFailed "Not implemented: validating when no excluded squares specified")
        | e::_ -> e in
    let rec flood (Coord (x,y) as coord) =
        let idx = coord_to_idx pmap coord in
        if not (Bitv.get check_map idx) then begin
        Bitv.set check_map idx true;
        [ Coord(x-1, y); Coord(x+1,y); Coord(x, y+1); Coord(x,y-1) ]
        |> List.filter ~f:(is_within_bounding_box pmap)
        |> List.iter ~f:flood
        end in
    flood some_empty;
    if not (Bitv.all_ones check_map) then
      raise (PuzzleFailed "Invalid Puzzlemap: Contains obstacles");
  in
  (* Not too large map *)
  if size > puzzle.max_size then
    raise (PuzzleFailed "Invalid Puzzlemap: too large");
  (* Not too small map *)
  let maxx, maxy =
    map |> Bitv.foldi_left (fun (maxx, maxy) idx is_marked ->
        match is_marked with
          | false -> (maxx, maxy)
          | true ->
             let Coord (x,y) = idx_to_coord pmap idx in
             (Int.max maxx x, Int.max maxy y)) (0,0) in
  let min_allowed_size = puzzle.max_size - (puzzle.max_size/10) in
  if maxx < min_allowed_size || maxy < min_allowed_size then
    raise (PuzzleFailed "Invalid Puzzlemap: too small");
  (* Not too sparse map *)
  let count_marked = count_marked pmap in
  if count_marked <= puzzle.max_size*puzzle.max_size / 5 + 1 then
    raise (PuzzleFailed "Invalid Puzzlemap: too sparse");
  ()

let validate_solution (puzzle: puzzle) (pmap: puzzle_map) (solution : solution) =
  (* NOTE: This checks only solution specific requirements. It is assumed that
   * the puzzle map generating the solution was itself validated. *)
  let (contour, bot, boosters) = solution in
  if not(is_marked pmap bot) then
    raise (PuzzleFailed "Invalid Solution: start bot");
  (* not too many or too few vertices in contour *)
  let nvertices = List.length contour in
  if nvertices < puzzle.vertex_min then
    raise (PuzzleFailed "Invalid Solution: Too few vertices");
  if nvertices > puzzle.vertex_max then
    raise (PuzzleFailed "Invalid Solution: Too many vertices");
  (* Correct number of boosters *)
  let () =
    let booster_reqs =
      [(Model.B, puzzle.manipulators); (Model.F, puzzle.fasts); (Model.L, puzzle.drills);
       (Model.R, puzzle.teleports); (Model.C, puzzle.clones); (Model.X, puzzle.spawn_points); ] in
    if booster_reqs
       |> List.exists ~f:(fun (code, count) ->
              let actual = List.count boosters ~f:(fun (code',_) -> code=code') in
              count <> actual) then
      raise (PuzzleFailed "Invalid Solution: Not correct number of boosters");
  in
  ()

let contour_of_marked (pmap : puzzle_map) =
  let PMap (_,map,_,_) = pmap in
  let Coord (llc_x, llc_y) as llc = 
    let llco =
      map |>
        Bitv.foldi_left (fun best idx is_marked ->
            let Coord(x,y) as coord = idx_to_coord pmap idx in
            match is_marked with
            | false -> best
            | true ->
               (match best with
                | None -> Some coord
                | Some (Coord (bx,by)) ->
                   if x < bx || (x = bx && y < by) then Some coord else best)
          ) None in
    match llco with
      | None -> raise (PuzzleFailed "No lower left hand corner found ?!")
      | Some llc -> llc in
  let rec contour ~(acc: coord * coord list) (con_coord : coord) (dir_prev : direction) =
    if con_coord = llc then
      let (last_corner, cont_rev) = acc in
      List.rev (last_corner::cont_rev)
    else
      let (dirr, tr), (dirl, tl) =
      let m = move_coord con_coord in
      match dir_prev with
          | Down  -> (Left,  m (-1, -1)), (Right, m ( 0,-1))
          | Right -> (Down,  m ( 0, -1)), (Up,    m ( 0, 0))
          | Up    -> (Right, m ( 0,  0)), (Left,  m (-1, 0))
          | Left  -> (Up,    m (-1,  0)), (Down,  m (-1,-1)) in
      let dir_new = 
      if not (is_marked pmap tl) then (* Turn left*)
          dirl
      else if is_marked pmap tr then (* Turn right *)
          dirr
      else (* Straight ahead *)
          dir_prev
      in
      let acc =
      if dir_new = dir_prev then acc
      else
          let (last_corner, cont_rev) = acc in
          (con_coord, last_corner::cont_rev)
      in
      contour ~acc:acc (move_coord_in_dir con_coord dir_new) dir_new
  in
  contour ~acc:(llc, []) (Coord (llc_x+1,llc_y)) Right

let solution_of_puzzle_map (puzzle: puzzle) (pmap : puzzle_map) : solution =
  let rec make_enough_vertices () =
    let contour = contour_of_marked pmap in
    let nvertex = List.length contour in
    if nvertex < puzzle.vertex_min then begin
      let amount = (puzzle.vertex_min - nvertex)/2 + 1 in
      grow_appendages pmap ~growths:amount ~girth:2;
      make_enough_vertices ()
      end
    else
      contour in
  let contour = make_enough_vertices () in
  let boosts_to_place = 
    [(Model.B, puzzle.manipulators); (Model.F, puzzle.fasts); (Model.L, puzzle.drills);
     (Model.R, puzzle.teleports); (Model.C, puzzle.clones); (Model.X, puzzle.spawn_points); ]
    |> List.map ~f:(fun (code, count) -> List.init count ~f:(fun _ -> code))
    |> List.concat
  in
  let total_free_spaces = 1 + List.length boosts_to_place in
  if total_free_spaces > List.length puzzle.include_coords then
    (* TODO!!! *)
    raise (PuzzleFailed "NotImplemented: We need more free spaces than force-included in spec");
  let bot, booster_spaces =
    match puzzle.include_coords with
      | [] -> raise (PuzzleFailed "Impossible. See above")
      | hd::rest -> (hd, List.take rest (List.length boosts_to_place)) in
  let boosters = List.zip_exn boosts_to_place booster_spaces in
  (contour, bot, boosters)

let string_of_solution ((contour, bot, boosters) : solution) =
  String.concat ~sep:","
    (List.map contour ~f:string_of_coord)
  ^"#"^
  string_of_coord bot
  ^"##"^ (* No obstacles *)
  String.concat ~sep:";"
    (List.map boosters ~f:(fun (code, coord) ->
         string_of_booster code ^ string_of_coord coord))

let attempt_puzzle (puzzle : puzzle) =
  let puzzle = 
    (* Hack for making sure final level is big enough: Add an include in
    upper-right-hand corner *)
    let rec try_next (Coord (x,y) as c) =
      if List.exists puzzle.exclude_coords ~f:((=) (Coord (x,y))) then
        if y > x then try_next (Coord (x, y-1))
                 else try_next (Coord (x-1, y))
      else
        { puzzle with include_coords = c::puzzle.include_coords } 
    in
    try_next (Coord (puzzle.max_size-1, puzzle.max_size-1))
  in
  (* Make a greedy path *)
  let pmap = new_puzzle_map puzzle in
  (* greedy_path pmap; *)
  spider_markable_path pmap;
  (* Grow girthy appendages *)
  let rec make_enough_marked () =
    let count = count_marked pmap in
    let limit = puzzle.max_size*puzzle.max_size / 5 + 1 in
    if count < limit then begin
      let missing = limit - count in
      let girth =
        let base = Float.to_int (Float.sqrt (Float.of_int missing)) in
        base + Random.int 4*base in
      let amount = missing/girth + 1 in
      grow_appendages pmap ~growths:amount ~girth:girth;
      make_enough_marked ()
      end
  in
  make_enough_marked ();
  (* Validate that the puzzlemap features are OK *)
  validate_puzzlemap puzzle pmap;
  (* Continue fixing while making a solution  *)
  let solution = solution_of_puzzle_map puzzle pmap in
  (* Validate and return *)
  validate_solution puzzle pmap solution;
  pmap, solution


let solve_puzzle (puzzle: puzzle) =
  let rec try_again i =
    if i < 100 then
      try 
        Some (attempt_puzzle puzzle)
      with PuzzleFailed _->
        try_again (i+1)
    else
      None
  in
  try_again 0
        


let loadPuzzle filename =
  let str = loadFile filename in
  let i = 0 in
  let (bNum, i) = read_int str i in
  assert (str @ i = ',');
  let (eNum, i) = read_int str (i+1) in
  assert (str @ i = ',');
  let (tSize, i) = read_int str (i+1) in
  assert (str @ i = ',');
  let (vMin, i) = read_int str (i+1) in
  assert (str @ i = ',');
  let (vMax, i) = read_int str (i+1) in
  assert (str @ i = ',');
  let (mNum, i) = read_int str (i+1) in
  assert (str @ i = ',');
  let (fNum, i) = read_int str (i+1) in
  assert (str @ i = ',');
  let (dNum, i) = read_int str (i+1) in
  assert (str @ i = ',');
  let (rNum, i) = read_int str (i+1) in
  assert (str @ i = ',');
  let (cNum, i) = read_int str (i+1) in
  assert (str @ i = ',');
  let (xNum, i) = read_int str (i+1) in
  assert (str @ i = '#');
  let (iSqs, i) = read_point_list str (i+1) in
  assert (str @ i = '#');
  let (oSqs, _) = read_point_list str (i+1) in
  { block_num = bNum;
    epoch  = eNum;
    max_size = tSize;
    vertex_min = vMin;
    vertex_max = vMax;
    manipulators = mNum;
    fasts = fNum;
    drills = dNum;
    teleports = rNum;
    clones = cNum;
    spawn_points = xNum;
    include_coords = List.map iSqs ~f:coord_of_pair;
    exclude_coords = List.map oSqs ~f:coord_of_pair; }


let puzzle_to_string (p : puzzle) =
  Printf.sprintf "{\n block_num: %d;\n epoch : %d;\n max_size: %d;\n vertex_min: %d;\n vertex_max: %d;\n manipulators: %d;\n fasts: %d;\n drills: %d;\n teleports: %d;\n clones: %d;\n spawn_points: %d;\n include_coords: %d points;\n exclude_coords: %d points; }"
    p.block_num
    p.epoch 
    p.max_size
    p.vertex_min
    p.vertex_max
    p.manipulators
    p.fasts
    p.drills
    p.teleports
    p.clones
    p.spawn_points
    (List.length p.include_coords)
    (List.length p.exclude_coords)


(* Unit tests *)
let%test "partition1" = 
  let t_coords = [Coord (0,0); Coord (0,1); Coord (1,0); Coord (1,1)] in
  let excl = [] in
  let expected = [[Coord (0,0); Coord (0,1); Coord (1,0); Coord (1,1)]] in
  let actual = partition t_coords (fun c -> not(List.mem excl c ~equal:(=))) in
  List.for_all2_exn expected actual ~f:(fun cl1 cl2 -> Set.equal (Coord.Set.of_list cl1) (Coord.Set.of_list cl2))

let%test "partition2" = 
  let t_coords = [Coord (0,0); Coord (0,1); Coord (1,0); Coord (1,1)] in
  let excl = [Coord (0,0); Coord (1,1)] in
  let expected = [[Coord (0,1)]; [Coord (1,0)]] in
  let actual = partition t_coords (fun c -> not(List.mem excl c ~equal:(=))) in
  List.for_all2_exn expected actual ~f:(fun cl1 cl2 -> Set.equal (Coord.Set.of_list cl1) (Coord.Set.of_list cl2))

let%test "partition3" = 
  let t_coords = [Coord (4,4); Coord (4,5); Coord (5,4); Coord (5,5)] in
  let excl = [Coord (4,5); Coord (5,4)] in
  let expected = [[Coord (4,4)]; [Coord (5,5)]] in
  let actual = partition t_coords (fun c -> not(List.mem excl c ~equal:(=))) in
  List.for_all2_exn expected actual ~f:(fun cl1 cl2 -> Set.equal (Coord.Set.of_list cl1) (Coord.Set.of_list cl2))

let%test "partition4" = 
  let t_coords = [Coord (0,2); Coord (1,2); Coord (2,2); 
                  Coord (0,1); Coord (1,1); Coord (2,1); 
                  Coord (0,0); Coord (1,0); Coord (2,0)] in
  let excl = [Coord (0,1); Coord (1,2)] in
  let expected = [[Coord (0,2)]; [Coord (0,0); Coord (1,0); Coord (2,0); Coord (1,1); Coord (2,1); Coord (2,2)]] in
  let actual = partition t_coords (fun c -> not(List.mem excl c ~equal:(=))) in
  (* Util.print_it [%sexp_of: coord list list] actual; *)
  List.for_all2_exn expected actual ~f:(fun cl1 cl2 -> Set.equal (Coord.Set.of_list cl1) (Coord.Set.of_list cl2))

let%test "partition5" = 
  let t_coords = [Coord (0,2); Coord (1,2); Coord (2,2); 
                  Coord (0,1); Coord (1,1); Coord (2,1); 
                  Coord (0,0); Coord (1,0); Coord (2,0)] in
  let excl = [Coord (0,1); Coord (1,2); Coord (2,1); Coord (1,0)] in
  let expected = [[Coord (0,2)]; [Coord (2,2)]; [Coord (1,1)]; [Coord (0,0)]; [Coord (2,0)]] in
  let actual = partition t_coords (fun c -> not(List.mem excl c ~equal:(=))) in
  (* Util.print_it [%sexp_of: coord list list] actual; *)
  List.for_all2_exn expected actual ~f:(fun cl1 cl2 -> Set.equal (Coord.Set.of_list cl1) (Coord.Set.of_list cl2))
