open Core
open SolutionFile
open Model
open SolverUtil

let rec repeat_action action n =
  if n <= 0 then [] else action :: (repeat_action action (n-1))

(* returns the actions to do the 180 degrees rotation and the optimal distance between the lanes *)
let rotate_180 bot move_direction =
  let broom = bot_broom bot in
  match bot.dir, move_direction with
  | Left,Up   | Right,Down -> ([E;E], broom.size_right * 2 + 1) (* Clockwise turn *)
  | Left,Down | Right,Up   -> ([Q;Q], broom.size_left * 2 + 1) (* Counter-clockwise turn *)
  | _ -> failwith "not supported previous action"

let urc_of_rect rect =
  let Coord(x_min,y_min) = rect.llc in
  Coord (x_min + rect.width - 1, y_min + rect.height - 1)

let solve (state : state) : solution =
  let open Sim.Monad in
  let rect_corners =
    let table = Coord.Table.create () in
    let rects = RectFinder.find_largest_unblocked_rects state.map 1 in
    List.iter rects ~f:(fun rect ->
      Hashtbl.set table ~key:rect.llc ~data:rect;
      Hashtbl.set table ~key:(urc_of_rect rect) ~data:rect;
    );
    table
  in
  let plan_actions actions = all_unit (List.map ~f:Sim.action actions) in
  let rec decide_as_master () =
    Sim.get_bot >>= fun bot ->
    if state.powerups.manipulators_collected > 0 then
      Sim.action (bot_extend_broom state bot) >>= fun () ->
      decide_as_master ()
    else
      match Hashtbl.is_empty rect_corners with
      | true -> return ()
      | false ->
          let heuristic c =
            Hashtbl.keys rect_corners
            |> List.map ~f:(fun corner -> manhattan_distance c corner)
            |> List.min_elt ~compare:Int.compare
            |> Option.value_exn
          in
          let astar_res = 
            astar_to_something 
              bot.coord 
              Int.max_value 
              (is_unblocked state.map) 
              (fun (c,_,_) -> heuristic c) 
              (Hashtbl.mem rect_corners) in
          match astar_res with
          | None -> failwith "Nowhere to go"
          | Some (target, actions, _) ->
              let rect = Hashtbl.find_exn rect_corners target in
              let urc = urc_of_rect rect in
              (* We've committed to mowing this rectangle, so we delete it from
               * the table. *)
              Hashtbl.remove rect_corners rect.llc;
              Hashtbl.remove rect_corners urc;
              let actions =
                (* Note: bot.direction is the same as after we've moved, so it's
                 * okay to inspect it here. *)
                if (bot.dir = Right && target = urc) ||
                  bot.dir = Left && target = rect.llc
                then actions @ [ Q; Q ]
                else actions
              in
              Util.print_it ~mes:"To get to rect" [%sexp_of: action list] actions;
              Util.print_it [%sexp_of: rectangle] rect;
              Out_channel.flush stdout;
              plan_actions actions >>= fun () ->
              mowe_that_lawn (rect.llc) (urc) (if target=rect.llc then Up else Down)
          
  and mowe_that_lawn (Coord(x_min, y_min) as llc) (Coord(x_max, y_max) as urc) direction =
    Sim.get_bot >>= fun bot ->
    let actions =
      match bot.dir, bot.coord with
      | Right, Coord (x,_) when x<x_max -> [D] 
      | Left, Coord (x,_) when x>x_min -> [A]
      | Right, Coord (_,y) | Left, Coord (_,y) ->
          let (rotation_actions, dist_to_next_lane) = rotate_180 bot direction in
          let horizontal_moves = 
            if direction = Up 
            then Int.min dist_to_next_lane (y_max-y)
            else Int.min dist_to_next_lane (y-y_min) in
          if horizontal_moves = 0 
          then []
          else
            let lane_actions = repeat_action (if direction=Up then W else S) horizontal_moves in
            lane_actions @ rotation_actions
      | _ -> [Q]
    in
    if actions = [] 
    then (
      Printf.printf "ouch\n";
      Out_channel.flush stdout; 
      decide_as_master () )
    else ( 
      let Coord (x,y) = bot.coord in
      Printf.printf "ouch (%d,%d)\n" x y;
      Util.print_it [%sexp_of: action list] actions;
      Out_channel.flush stdout;
      plan_actions actions >>= fun () ->
      mowe_that_lawn llc urc direction)
  in 
  let () = Sim.run state (decide_as_master ()) in
  Sim.unsafe_make_solution state
