APP_ML_FILES = $(wildcard app/*.ml)
APP_EXE_FILES = $(APP_ML_FILES:.ml=.exe)
SOLVE_EXE = _build/default/app/solve.exe
SCORE_FACTOR_EXE = _build/default/app/score_factor.exe

PROBLEMS = $(wildcard problems/*.desc)

all:
	dune build $(APP_EXE_FILES)

clean:
	dune clean

test:
	dune runtest

solutions: \
  $(PROBLEMS:problems/%.desc=solutions/%.sol) \
  $(PROBLEMS:problems/%.desc=solutions/%.C.sol)

.PHONY: solutions.zip
solutions.zip:
	mkdir solutions_zip
	rm -f solutions.zip
	#./get_wallet.sh # live task ended
	./best_buys C 2000 "`cat wallet`" solutions solutions_zip
	cd solutions_zip && zip -q ../$@ *.sol *.buy
	rm -rf solutions_zip

solutions/%.sol: $(SOLVE_EXE) problems/%.desc
	$(SOLVE_EXE) $(SOLVE_FLAGS) -o tmp_$@ problems/$*.desc
	@if ./compare_solutions $@ tmp_$@; then \
	  cp -f tmp_$@ $@;\
	else \
	  touch  $@; \
	fi;

solutions/%.C.sol: $(SOLVE_EXE) problems/%.desc
	$(SOLVE_EXE) $(SOLVE_FLAGS) -b buy_files/C -o tmp_$@ problems/$*.desc
	@if ./compare_solutions $@ tmp_$@; then \
	  cp -f tmp_$@ $@;\
	else \
	  touch  $@; \
	fi;

problems/%.scfac: $(SCORE_FACTOR_EXE) problems/%.desc
	$(SCORE_FACTOR_EXE) -o $@ problems/$*.desc

.PHONY: scorefactors
scorefactors: $(PROBLEMS:%.desc=%.scfac)

tmp_solutions/%.sol: $(SOLVE_EXE) problems/%.desc
	$(SOLVE_EXE) $(SOLVE_FLAGS) -o $@ problems/$*.desc

tmp_solutions/%.C.sol: $(SOLVE_EXE) problems/%.desc
	$(SOLVE_EXE) $(SOLVE_FLAGS) -b buy_files/C -o $@ problems/$*.desc
