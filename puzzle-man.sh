#!/bin/bash
BLOCKS=lambda-client/blocks
PUZZLE=_build/default/app/puzzle.exe
SOLVE=_build/default/app/solve.exe
while true; do
    echo "I'm Awake"
    for i in $(ls $BLOCKS); do
        dir=$BLOCKS/$i
        if ! [ -e $dir/attempted ]; then
            echo "Attempting $i"
            touch $dir/attempted
            if $SOLVE -o $dir/task.sol $dir/task.desc && $PUZZLE -o $dir/puzzle.desc $dir/puzzle.cond; then
                echo "Submitting $i"
                (cd lambda-client; ./lambda-cli.py submit $i ../$dir/task.sol ../$dir/puzzle.desc)
            fi
        fi
    done
    sleep 60;
done
    
