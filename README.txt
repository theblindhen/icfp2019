The Blind Hen 2019 ICFP Contest submission
==========================================

Team
----

Johan S. H. Rosenkilde
Jonas B. Jensen
Christoffer R. Nielsen

All in Copenhagen, Denmark

How to build
------------

First install the "opam" package manager and configure it as follows:

    $ sudo apt-get install opam
    $ opam init # Answer `y` at the prompt
    $ opam switch 4.07.1   # 4.07.0 also works
    follow instructions after previous command if run

Install packages we need:

    $ sudo apt-get install autoconf
    $ opam install merlin core dune yojson bitv oseq

Now build and run this program:

    $ make
    $ make -j$NUM_CPUS solutions
    $ make solutions.zip

On subsequent terminal sessions, do the following unless you allowed `opam init`
to modify your shell rc:

    $ eval `opam config env`

Strategy for worker-wrappers
----------------------------

A robot is controlled by a monadic program that yields actions and can
spawn other robots, giving the spawned robot another program. Programs can
inspect shared state to query the map and to coordinate work between them.

We partition the map into components (example: 20 components for a 150x150 map).
The first robot on the map (_master_) first runs across the map to pick up
boosters, taking an efficient but not optimal route. It will also go to X-points
to clone itself when it has a C-booster. All clones are _slaves_, and the master
also becomes a slave when it's done taking boosters. A slave will assign itself
to a component that's near its current component and try to wrap that entire
component. The slave will stay within that component, and other slaves won't
make an effort to work on that component.

To wrap a component, the robot iterates a loop where it picks a good location
and moves there. First, it will consider what happens in each of the four
directions it can move, using a scoring system that rewards moving its arms into
narrow corners. If one of those four moves is sufficiently good, it will make
that move. Otherwise, it will move to the nearest location in the component that
isn't wrapped. As a tie breaker if there are multiple best locations, the robot
will try to move away from other robots. After moving, the robot applies a few
heuristics to decide whether to turn and whether to step sideways.

After we started getting lambda coins, we decided how to spend them by solving
each map twice: once with an initial cloning booster and once without. For each
map, we estimated how many points we would get by picking the solution with the
initial booster. Then we bought initial cloning boosters for as many of the most
valuable maps as we could afford.

We kept the best solution for each problem, with and without clone, in our Git
repository. Improvements to the strategy would often improve the score on only
two thirds of the maps, so keeping old solutions ensured that we could make such
changes safely.

We had a "lawn mower" solver that wrap large rectangles with adjacent stripes,
but we weren't able to integrate it with the rest of the solver in time. The
plan was to make the _master_ robot, with all its added manipulators, wrap all
large rectangles using that strategy.

Strategy for puzzles
--------------------

Unlike the majority of tasks we saw on the block chain, our puzzle solver
produces maps with very little area to wrap. They were maze-like in nature. We
couldn't always make such a map within the constraints, but it worked out at
least 90% of the time.
