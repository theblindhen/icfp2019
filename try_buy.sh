#!/bin/bash
BUY=$1
for p in $(cd problems; ls *.desc); do
    PSOL=${p%.desc}.sol 
    echo Problem $p
    ./_build/default/app/solve.exe -o tmp_solutions/$PSOL -b $BUY problems/$p
    ./compare_solutions solutions/$PSOL tmp_solutions/$PSOL
done
