open Core
open Blindhen
open Model

let log2 x =
  Float.log x /. Float.log 2.

let command =
  let open Command.Let_syntax in
  Command.basic
    ~summary:"Score factor for problem"
    [%map_open
      let problem_file = anon ("PROBLEM_FILE" %: string)
      and output_file_opt =
        flag "-o" (optional string) ~doc:"FILENAME output file"
      in
      fun () ->
        let state =
          TaskFile.loadProblem problem_file None
          |> Model.state_from_task
        in
        let size_x, size_y = model_true_size state.map in
        let score_factor =
          1000 * Float.to_int (Float.round_up (log2 (Float.of_int
            (size_x * size_y)
          )))
        in
        match output_file_opt with
        | None -> Printf.printf "%d" score_factor
        | Some output_file ->
          Out_channel.write_all output_file ~data:(string_of_int score_factor)
    ]

let () =
  Command.run command
