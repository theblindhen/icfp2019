open Core
open Blindhen
open Sim
open Model

let command =
  let open Command.Let_syntax in
  Command.basic
    ~summary:"Solve ICFP19 problem"
    [%map_open
      let problem_file = anon ("PROBLEM_FILE" %: string)
      and output_file_opt =
        flag "-o" (optional string) ~doc:"FILENAME output file"
      and buy_file_opt =
        flag "-b" (optional string) ~doc:"FILENAME purchase file"
      and use_alt_ai_opt =
        flag "-a" (optional string) ~doc:"alternative ai"
      and print_map =
        flag "-p" no_arg ~doc:"print map first"
      in
      fun () ->
        let state =
          TaskFile.loadProblem problem_file buy_file_opt
          |> Model.state_from_task
        in
        if print_map then (
          Printf.printf "\nMap:\n%s\n\n" (Model.string_of_state state);
        );
        let solver = match use_alt_ai_opt with
                    | Some "L" -> LawnMower.solve
                    | Some "N" -> NearestSolver.solve ~rotation:`forward
                    | Some "C" -> ComponentSolver.solve ~ignore_components:false ~ignore_rects:true
                    | Some "CI" -> ComponentSolver.solve ~ignore_components:true ~ignore_rects:true
                    | Some "CR" -> ComponentSolver.solve ~ignore_components:false ~ignore_rects:false
                    | Some _ -> failwith "Unknown AI"
                    | None -> ComponentSolver.solve ~ignore_components:false ~ignore_rects:true
        in
        let () =
          if print_map then
            if use_alt_ai_opt = Some "CR" then
              let (rect_map, component_map) = ComponentSolver.make_component_map state false in
              Printf.printf "\nComponent Map:\n%s\n\n" (Components.string_of_component_map state component_map);
            else
              let (rect_map, component_map) = ComponentSolver.make_component_map state true in
              Printf.printf "\nComponent Map:\n%s\n\n" (Components.string_of_component_map state component_map);
        in
        let solution =  solver state in
        let solution_string = SolutionFile.string_of_solution solution in
        match output_file_opt with
        | None -> Printf.printf "%s" solution_string
        | Some output_file ->
          Out_channel.write_all output_file ~data:solution_string
    ]

let () =
  Command.run command
