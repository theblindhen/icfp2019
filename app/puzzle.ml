open Core
open Blindhen
open Sim
open Model

let command =
  Random.init 3141592;
  let open Command.Let_syntax in
  Command.basic
    ~summary:"Solve Puzzle"
    [%map_open
      let puzzle_file = anon ("PUZZLE_FILE" %: string)
      and output_file_opt =
        flag "-o" (optional string) ~doc:"FILENAME output file (desc)"
      in
      fun () ->
        let puzzle = Puzzle.loadPuzzle puzzle_file in
        Printf.printf "\nLoaded Puzzle: %s\n" (Puzzle.puzzle_to_string puzzle);
        match Puzzle.solve_puzzle puzzle with
        | Some (_, solution) ->
           Printf.printf "Puzzle solved!\n";
           let solution_string = Puzzle.string_of_solution solution in
           begin
           match output_file_opt with
           | None -> Printf.printf "%s" solution_string
           | Some output_file ->
              Out_channel.write_all output_file ~data:solution_string
           end
        | None ->
           Printf.printf "I had to give up, sorry :-( \n"
        (* let solution = Puzzle.solve puzzle in
         * run state solution;
         * let solution_string = Puzzle.string_of_solution solution in
         * match output_file_opt with
         * | None -> Printf.printf "%s" solution_string
         * | Some output_file ->
         *   Out_channel.write_all output_file ~data:solution_string *)
    ]

let () =
  Command.run command
